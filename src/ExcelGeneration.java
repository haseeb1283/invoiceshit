import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import model.Bean;
import model.Database;
import model.Utils;

public class ExcelGeneration {

	private final String HEADERS[] = {"Debiteurennr", "Code", "Valuta", "Factuurdatum", "Periode", "Factuurnummer", "Vervaldatum",
			"Nummer", "Grtboekrek.", "Rel/KPL", "Prj/activa", "Bedrag", "debit_credit", "Omschrijving", "Btwcode",
			"goods/services", "Landcode", "Vatnumber", "Uitvoeringsdatum" };

	private static final int COLUMN_WIDTH[] = { 15,6, 6, 15, 10, 8, 15, 10, 8, 10, 10, 14, 12, 25, 10, 10, 5, 15, 15 };

	private final String FILE_NAME;
	private final String SHEET_NAME = "Sheet 1";

	public ExcelGeneration(String filename) {
		this.FILE_NAME = filename;
	}

	public void generateFile(Date startDate, Date endDate) {
		try {
			// creating an instance of HSSFWorkbook class
			XSSFWorkbook workbook = new XSSFWorkbook();
			// invoking creatSheet() method and passing the name of the sheet to be created
			XSSFSheet sheet = workbook.createSheet(SHEET_NAME);
			for (int i = 0; i < COLUMN_WIDTH.length; i++) {
				sheet.setColumnWidth(i, COLUMN_WIDTH[i] * 256);
			}
			createHeaderRow(workbook, sheet);
			insertDataToFile(workbook, sheet, startDate, endDate);
			// rowhead.setRowStyle(headerCellStyle);
			FileOutputStream fileOut = new FileOutputStream(FILE_NAME);
			workbook.write(fileOut);
			// closing the Stream
			fileOut.close();
			// closing the workbook
			workbook.close();
			// prints the message on the console
			System.out.println("Excel file has been generated successfully.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createHeaderRow(XSSFWorkbook workbook, XSSFSheet sheet) {
		// creating the 0th row using the createRow() method
		XSSFRow rowhead = sheet.createRow((short) 0);
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// fill foreground color ...
		headerCellStyle.setFillBackgroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		headerCellStyle.setFillForegroundColor(IndexedColors.YELLOW.index);
		// and solid fill pattern produces solid grey cell fill
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		;
		headerCellStyle.setBottomBorderColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headerCellStyle.setBorderLeft(BorderStyle.THIN);
		;
		headerCellStyle.setLeftBorderColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		;
		headerCellStyle.setRightBorderColor(IndexedColors.GREY_25_PERCENT.getIndex());
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		;
		headerCellStyle.setTopBorderColor(IndexedColors.GREY_25_PERCENT.getIndex());
		// headerCellStyle.setFont(headerFont);

		// creating cell by using the createCell() method and setting the values to the
		// cell by using the setCellValue() method

		Font headerFont = workbook.createFont();
		headerFont.setFontName("Arial");
		headerFont.setFontHeight((short) 195);

		headerFont.setBold(true);

		for (int i = 0; i < HEADERS.length; i++) {
			createCell(rowhead, HEADERS[i], headerCellStyle, i, headerFont);
		}
	}

	private void insertDataToFile(XSSFWorkbook workbook, XSSFSheet sheet, Date startDate, Date endDate) {
		Database db = Database.getInstance();
		ArrayList<Bean> data = db.getAllData();
		int recordNo = 1;
		Font font = workbook.createFont();
		font.setFontName("Courier New");
		font.setColor(IndexedColors.GREY_80_PERCENT.getIndex());
		font.setFontHeight((short) 200);

		for (Bean bean : data) {
			if (!isInRange(startDate, endDate, bean)) {
				continue;
			}
			XSSFRow row = sheet.createRow((short) recordNo++);

			int temp = 0;

			// create eighth cell
			XSSFCell cell44 = createCell(row, String.valueOf(bean.getInvoiceNumber()),
					sheet.getWorkbook().createCellStyle(), temp++, font);
			cell44.setCellValue(bean.getCustomerNumber());
			// create first cell
			createCell(row, "VRK", sheet.getWorkbook().createCellStyle(), temp++, font);
			// create second cell
			createCell(row, "EUR", sheet.getWorkbook().createCellStyle(), temp++, font);

			// create third date cell
			XSSFCell cell = createDateCell(workbook, row, temp++, "", font, "dd/MM/yyyy");
			cell.setCellValue(Utils.getDateFullFormatDate(bean.getInvoiceDate()));

			// create fourth cell
			createCell(row, Utils.getDisplayDateInMonthsYear(bean.getInvoiceDate()),
					sheet.getWorkbook().createCellStyle(), temp++, font);

			// create fifth cell
			XSSFCell cell1 = createCell(row, String.valueOf(bean.getInvoiceNumber()),
					sheet.getWorkbook().createCellStyle(), temp++, font);
			cell1.setCellValue(bean.getInvoiceNumber());

			// create sixth date cell
			XSSFCell cell3 = createDateCell(workbook, row, temp++, "", font, "dd/MM/yyyy");
			cell3.setCellValue(
					Utils.getDateInSimpleFormat(Utils.addDates(Utils.getDisplayDate(bean.getInvoiceDate()), 7).trim()));

			// create seventh cell
			createCell(row, "", sheet.getWorkbook().createCellStyle(), temp++, font);

			// create eighth cell
			XSSFCell cell4 = createCell(row, String.valueOf(bean.getInvoiceNumber()),
					sheet.getWorkbook().createCellStyle(), temp++, font);
			cell4.setCellValue(8500);

			// create ninth cell
			createCell(row, "", sheet.getWorkbook().createCellStyle(), temp++, font);

			// create tenth cell
			createCell(row, "", sheet.getWorkbook().createCellStyle(), temp++, font);

			CellStyle style = workbook.createCellStyle();
			style.setAlignment(HorizontalAlignment.RIGHT);
			// create eleventh cell
			XSSFCell cell5 = createCell(row, String.valueOf(bean.getInvoiceNumber()), style, temp++, font);
			cell5.setCellValue(String.format(Locale.GERMAN, "%.2f", bean.getAllCostCombined()));

			// create twelfth cell
			createCell(row, "debit", sheet.getWorkbook().createCellStyle(), temp++, font);

			// create thirteenth cell
			createCell(row, Database.getInstance().getCompanyDetails(Utils.COMPANY_ID).getReinigen(),
					sheet.getWorkbook().createCellStyle(), temp++, font);

			// create fourteenth cell
			XSSFCell cellBTW = createCell(row, "", sheet.getWorkbook().createCellStyle(), temp++, font);
			if (!bean.getVatNumber().trim().isEmpty()) {
				cellBTW.setCellValue("ICP");
			}

			// create fifteenth cell
			createCell(row, "goods", sheet.getWorkbook().createCellStyle(), temp++, font);

			// create sixteenth cell
			createCell(row, bean.getCountry().equals("Nederland") ? "NL" : "BE", sheet.getWorkbook().createCellStyle(),
					temp++, font);
			// create seventeenth cell
			XSSFCell cell6 = createCell(row, "", sheet.getWorkbook().createCellStyle(), temp++, font);
			cell6.setCellValue(bean.getVatNumber());

			// create seventeenth cell
			createCell(row, "", sheet.getWorkbook().createCellStyle(), temp++, font);

		}

	}

	private Date getDate(String date) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			return format.parse(date);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	private boolean isInRange(Date startDate, Date endDate, Bean bean) {
		System.out.println("Invoice date: " + bean.getInvoiceDate());
		Date toCompareDate = getDate(bean.getInvoiceDate());
		System.out.println("Start date: " + startDate);
		System.out.println("End date: " + endDate);
		System.out.println("Comparing date: " + toCompareDate);

		System.out.println(toCompareDate.compareTo(startDate));
		System.out.println(toCompareDate.compareTo(endDate));

		return toCompareDate.compareTo(startDate) >= 0 && toCompareDate.compareTo(endDate) <= 0;
	}

	private XSSFCell createDateCell(Workbook wb, XSSFRow row, int index, String value, Font font, String format) {
		CellStyle cellStyle = wb.createCellStyle();
		CreationHelper createHelper = wb.getCreationHelper();
		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(format));
		XSSFCell cell = row.createCell(index);

		cell.setCellStyle(cellStyle);
		cellStyle.setFont(font);
		cell.setCellValue(value);

		return cell;
	}

	private XSSFCell createCell(XSSFRow row, String text, CellStyle style, int index, Font font) {
		XSSFCell cell = row.createCell(index);
		cell.setCellValue(text);
		if (style != null) {
			if (font != null) {
				style.setFont(font);
			}
			cell.setCellStyle(style);
		}
		return cell;
	}
}