
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import model.Bean;
import model.Company;
import model.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 *
 * @author Haseeb
 */
public class PdfGenerator {

	private static PdfGenerator instance;

	public static PdfGenerator getInstance() {
		if (instance == null) {
			instance = new PdfGenerator();
		}
		return instance;
	}

	private PdfGenerator() {

	}

	public String generatePdf(Bean bean, Company company) {

		String path = Utils.INVOICES_PATH + "INVOICE_" + bean.getInvoiceNumber() + ".pdf";
		try {
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(path));
			document.open();

			PdfPTable table = new PdfPTable(7);
			table.setWidthPercentage(100);
			table.getDefaultCell().setFixedHeight(75);
			float[] tablesWidth = new float[] { 14.7f, 20.9f, 16.3f, 16.3f, 12.3f, 10.8f, 12.7f };

			table.setWidths(tablesWidth);

			// image row
			setFirstRow(table, company.getPhotoPath().getAbsolutePath());
			setEmptyRow(table);
			setEmptyRow(table);

			setFacturerRow(table, company.getName());
			setCountryRow(table, company.getCountry1());
			// title row
			setSecondRow(table, company.getAddress1());
			setCountryRow2(table, company.getCountry2());
			setThirdRow(table, bean.getCompanyName().trim(), company.getKvk());
			setTAVRow(table, bean.getPersonName().trim(), company.getBtw());
			setColumnDRow(table, bean.getAddress().trim());
			setColumnEFRow(table, bean.getZipcode().trim(), bean.getCity().trim(), company.getBank());
			setIBANRow(table, bean.getCountry().trim(), company.getIban());
			setVATRow(table, String.valueOf(bean.getVatNumber()), company.getBic());
			if (!company.getTel1().isEmpty())
				setEmptyBorderRow(table, company.getTel1().trim().isEmpty() ? "" : "Tel 1: ", company.getTel1().trim());
			if (!company.getTel2().isEmpty()) {
				setEmptyBorderRow(table, company.getTel2().trim().isEmpty() ? "" : "Tel 2: ", company.getTel2().trim());
			}
			if (!company.getSenderEmail().isEmpty()) {
				setEmptyBorderRow(table, company.getSenderEmail().trim().isEmpty() ? "" : "Email: ",
						company.getSenderEmail().trim());
			}
			
			if (!company.getWebsite().isEmpty()) {
				setEmptyBorderRow(table, company.getWebsite().trim().isEmpty() ? "" : "",
						company.getWebsite().trim());
			}
			// senders email row
			// setEmptyBorderRow(table, "", "");
			setClosingBox(table, company.getTagLine());
			setEmptyRow(table);
			setFifteenthRow(table);
			setSixteenthRow(table, Utils.getDisplayDate(bean.getInvoiceDate().trim()),
					String.valueOf(bean.getInvoiceNumber()),
					Utils.addDates(Utils.getDisplayDate(bean.getInvoiceDate()), 7).trim());
			setEmptyRow(table);
			setEighteenthRow(table, "OMSCHRIJVING", "AANTAL", "STUKSPRIJS", "SUBTOTAAL");
			setValuesRow(table, bean.getItemOneDescription(), false, bean.getAmountOfItemsOne(), bean.getPriceItemOne(),
					bean.getAmountOfItemsOne() * bean.getPriceItemOne());
			setValuesRow(table, bean.getItemTwoDescription(), false, bean.getAmountOfItemsTwo(), bean.getPriceItemTwo(),
					bean.getAmountOfItemsTwo() * bean.getPriceItemTwo());
			setValuesRow(table, bean.getItemThreeDescription(), false, bean.getAmountOfItemsThree(),
					bean.getPriceItemThree(), bean.getAmountOfItemsThree() * bean.getPriceItemThree());

			setValuesRow(table, " ", false, 0, 0, 0);

			setValuesRow(table, " ", false, 0, 0, 0);

			setEighteenthRow(table, "VERZENDKOSTEN", "AANTAL", "STUKSPRIJS", "SUBTOTAAL");
			setValuesRow(table, bean.isHasShipmentCost() ? "Ja" : "Nee", true,
					Integer.parseInt(bean.getShipmentCostNumber()), bean.getShipmentCost(),
					Integer.parseInt(bean.getShipmentCostNumber()) * bean.getShipmentCost());

			setTotalExclBtw(table, String.format("€%8.2f", bean.getAllCostCombined()));
			setBTWrow(table, bean.getVatPercentage());
			setTotalBTWrow(table, String.format("€%8.2f", (bean.getAllCostCombined() / 100) * bean.getVatPercentage()));
			setTotalBEETALENWrow(table, String.format("€%8.2f",
					((bean.getAllCostCombined() / 100) * bean.getVatPercentage()) + bean.getAllCostCombined()));
			setEmptyRow(table);
			setEmptyRow(table);
			setEmptyRow(table);
			setEmptyRow(table);
			setOneLastCellRow(table);
			setSecondLastRow(table,
					"Wij verzoeken u vriendelijk het bedrag binnen 7 dagen over te maken o.v.v het factuurnummer");
			setSecondLastRow(table,
					"Op alle diensten zijn onze algemene voorwaarden van toepassing. Deze kunt u downloaden van onze website");
			setSecondLastRow(table, "Bij BTW tarief GEEN of 0% geldt: BTW verlegd naar medecontractant.");

			/*
			 * //first 3 col table row
			 * setFourthRow(table,data.get(0).getMemberName().replaceAll("\"", "")); //empty
			 * row setFifthRow(table, data.get(0).getAddress().replaceAll("\"", ""));
			 * //setEmptyRowBordered(table); setSixthRow(table,
			 * data.get(0).getZipCode().replaceAll("\"", "") + " " +
			 * data.get(0).getCity().replaceAll("\"", "") ); setSeventhRow(table);
			 * setEighthRow(table); setNinthRow(table); setTenthRow(table);
			 * setBankExtraRow(table); //empty row
			 * 
			 * setEmptyRowBordered(table); //webist row setEleventhRow(table);
			 * 
			 * //email row setTwelthRow(table);
			 * 
			 * //set reserven row setThirteenthRow(table);
			 * 
			 * //set Fiftheenth row setFifteenthRow(table); String todayDate = getDate(new
			 * Date()); setSixteenthRow(table, todayDate, invoiceNumber,
			 * get14DateLaterDate(todayDate, 14));
			 * 
			 * //empty row setEmptyRow(table);
			 * 
			 * 
			 * //setEighteenth row if (!isAnnual) { setEighteenthRow(table, "Periode",
			 * "SERVICE", "AANTAL KM", "PRIJS PER KM", " SUBTOTAAL"); } else {
			 * setEighteenthRow(table, "DATUM", "LID SINDS", "AANTAL JAREN LID",
			 * "LIDMAATSCHAP PER JAAR", " SUBTOTAAL"); }
			 * 
			 * double totalAmountExcl = 0;
			 * 
			 * if (!isAnnual) { int total = 0; String service = ""; for (int i = 0; i <
			 * data.size(); i++) { if(data.get(0) instanceof D2dDataClass){ total +=
			 * ((D2dDataClass)data.get(i)).getDistance(); service = "Deur tot deur";
			 * //D2dDataClass bean = (D2dDataClass) data.get(i); } else if(data.get(0)
			 * instanceof LijndiestDataClass){ service = "Lijndienst"; LijndiestDataClass
			 * bean = (LijndiestDataClass) data.get(i); total +=
			 * Integer.parseInt(bean.getmLijndiest()); } }
			 * 
			 * setValuesRow(table, MONTHS_NAMES[selectedMonth-1], service,
			 * String.valueOf(total), String.format(Locale.forLanguageTag("nl"), "€ %.2f",
			 * Double.parseDouble(pricePerKm)), String.format(Locale.forLanguageTag("nl"),
			 * "€ %.2f", total Double.parseDouble(pricePerKm)));
			 * 
			 * totalAmountExcl = total * Double.parseDouble(pricePerKm); } else {
			 * AnnualDataClass bean = (AnnualDataClass) data.get(0); int years =
			 * getYearsDifference(new Date(), bean.getMemberSince(), weeks); totalAmountExcl
			 * = Double.parseDouble(annualPrice);
			 * //System.out.println(data.get(0).getMemberSince()); setValuesRow(table,
			 * changeToCurrentYear(bean.getMemberSince()),
			 * getCorrectDateFormat(bean.getMemberSince()), String.valueOf(years),
			 * String.format(Locale.forLanguageTag("nl"), "€ %.2f",
			 * Double.parseDouble(annualPrice)), String.format(Locale.forLanguageTag("nl"),
			 * "€ %.2f", totalAmountExcl)); }
			 * 
			 * //set total exclue btw. setTotalExclBtw(table, String.format(Locale.GERMAN,
			 * "€ %.2f", totalAmountExcl)); //set btw% row setBTWrow(table);
			 * 
			 * /*Output output = data.get(0); //set Total Btw row double vat = (9.0 / 100) *
			 * totalAmountExcl;
			 * 
			 * setTotalBTWrow(table, String.format(Locale.GERMAN, "€ %.2f", vat));
			 * 
			 * //set total row setTotalRow(table, String.format(Locale.GERMAN, "€ %.2f",
			 * vat + totalAmountExcl)); setEmptyRow(table); setEmptyRow(table);
			 * 
			 * //set Bold Title row setOneLastCellRow(table);
			 * 
			 * //set second last row setSecondLastRow(table);
			 * 
			 * 
			 * //String lastRowText = "";
			 * 
			 * if(isAnnual){ // lastRowText =
			 * "Het bedrag op de factuur per rit is exclusief wettelijke 9% BTW"; } else{ //
			 * lastRowText =
			 * "Het bedrag op de factuur per rit is exclusief wettelijke 9% BTW"; }
			 * setLastRow(table);
			 */
			document.add(table);
			document.close();

			// bean.setInvoiceDate(Utils.getTodayDate());
			return path;

			/*
			 * new Thread(new Runnable() {
			 * 
			 * @Override public void run() {
			 * 
			 * String email = data.get(0).getEmail(); //String email = "b-art_@live.nl";
			 * if(email.equals("geenmail@belbusnoordkop.nl")){ email =
			 * "invoice@belbusnoordkop.nl"; }
			 * 
			 * 
			 * if(isTestMail){ email = testEmailAddress; }
			 * 
			 * if(email.isEmpty()){ System.err.println("Wrong Email Adress" +
			 * data.get(0).getMemberId()); }
			 * 
			 * //System.err.println(email); //System.out.println(email);
			 * SendEmail.sendMail(email, pdfFileName, isAnnual,
			 * String.valueOf(data.get(0).getMemberId()), mailProgress, mailSent, label,
			 * type); } }).start(); ++total;
			 * 
			 * 
			 * 
			 * //mailSent.setText(String.valueOf(totalSent) + "/" +
			 * String.valueOf(++total)); mailProgress.setMaximum(total);
			 * 
			 * /*if(totalSent == total){ mailProgress.setVisible(false);
			 * mailSent.setVisible(false); label.setVisible(false);
			 * mailProgress.setValue(0); totalSent = 0; total = 0; }
			 */

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private String getCurrentDate() {
		try {
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			return format.format(date);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private String changeToCurrentYear(String date) {
		try {

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date changedDate = format.parse(date);
			Date newDate = new Date();
			changedDate.setYear(newDate.getYear());
			format = new SimpleDateFormat("dd-MM-yyyy");
			return format.format(changedDate);
		} catch (Exception e) {
			e.printStackTrace();
			return date;
		}
	}

	private String getCorrectDateFormat(String date) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date changedDate = format.parse(date);
			format = new SimpleDateFormat("dd-MM-yyyy");
			return format.format(changedDate);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private int getYearsDifference(Date currentDate, String memberSince, int weeks) {
		try {
			memberSince = getCorrectDateFormat(memberSince);
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

			String tempString = get14DateLaterDate(format.format(currentDate), weeks * 7);
			Date sinceDate = format.parse(memberSince);
			Date newCurrentDate = format.parse(tempString);

			// System.out.println(format.format(sinceDate) + format.format(newCurrentDate));
			return Math.abs(getDiffYears(sinceDate, newCurrentDate));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getDiffYears(Date first, Date last) {
		Calendar a = getCalendar(first);
		Calendar b = getCalendar(last);
		int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
		if (a.get(Calendar.DAY_OF_YEAR) > b.get(Calendar.DAY_OF_YEAR)) {
			diff--;
		}
		return diff;
	}

	public Calendar getCalendar(Date date) {
		Calendar cal = Calendar.getInstance(Locale.US);
		cal.setTime(date);
		return cal;
	}

	private String getDate(Date date) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			return format.format(date);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private String get14DateLaterDate(String dateString, int days) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			Date currentDate = format.parse(dateString);
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, currentDate.getDate() + days);
			Date newDate = calendar.getTime();
			// System.out.println(format.format(newDate));
			return format.format(newDate);
		} catch (Exception e) {
			e.printStackTrace();
			return dateString;
		}
	}

	private void setLastRow(PdfPTable table) {
		try {
			Paragraph pg = new Paragraph();
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.setColspan(7);
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(" "));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setSecondLastRow(PdfPTable table, String text) {
		try {
			Paragraph pg = new Paragraph(text);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.setColspan(7);
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setOneLastCellRow(PdfPTable table) {
		try {
			Paragraph pg = null;

			pg = new Paragraph("Opmerkingen & voorwaarden");
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.setColspan(7);
			cellItem.setPaddingRight(80f);
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cellItem);

			// table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setTotalRow(PdfPTable table, String amount) {
		try {
			Paragraph pg = new Paragraph("TE BETALEN");
			pg.setAlignment(Element.ALIGN_RIGHT);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;
			PdfPCell cellItem = new PdfPCell();
			cellItem.setColspan(5);
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(cellItem);

			pg = new Paragraph(amount);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.addElement(pg);
			// cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(" "));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setTotalBTWrow(PdfPTable table, String amount) {
		try {
			Paragraph pg = new Paragraph("TOTAAL BTW");
			pg.setAlignment(Element.ALIGN_RIGHT);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;
			PdfPCell cellItem = new PdfPCell();
			cellItem.setColspan(5);
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(cellItem);

			pg = new Paragraph(amount);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.addElement(pg);
			// cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(" "));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setTotalBEETALENWrow(PdfPTable table, String amount) {
		try {
			Paragraph pg = new Paragraph("TE BETALEN");
			pg.setAlignment(Element.ALIGN_RIGHT);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;
			PdfPCell cellItem = new PdfPCell();
			cellItem.setColspan(5);
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(cellItem);

			pg = new Paragraph(amount);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.addElement(pg);
			// cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(" "));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setBTWrow(PdfPTable table, double percentage) {
		try {
			Paragraph pg = new Paragraph("BTW%");
			pg.setAlignment(Element.ALIGN_RIGHT);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;
			PdfPCell cellItem = new PdfPCell();
			cellItem.setColspan(5);
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(cellItem);

			pg = new Paragraph(String.format("%8.2f%%", percentage));
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.addElement(pg);
			// cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(" "));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setTotalExclBtw(PdfPTable table, String amount) {
		try {
			Paragraph pg = new Paragraph("TOTAAL EXCL. BTW");
			pg.setAlignment(Element.ALIGN_RIGHT);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;
			PdfPCell cellItem = new PdfPCell();
			cellItem.setColspan(6);
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(cellItem);

			pg = new Paragraph(amount);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.addElement(pg);
			// cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(" "));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setValuesRow(PdfPTable table, String description, boolean isDescriptionCenter, int amount,
			double price, double totalPrice) {
		try {

			// first column (Description)
			Paragraph pg = new Paragraph(description);
			if (!isDescriptionCenter) {
				pg.setAlignment(Element.ALIGN_LEFT);
			} else {
				pg.setAlignment(Element.ALIGN_CENTER);
			}
			pg.setFont(FontFactory.getFont("Arial", 7, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setExtraParagraphSpace(10);
			cellItem.setColspan(4);
			table.addCell(cellItem);

			String amountInString = "";
			if (price == 0) {
				amountInString = "";
			} else {
				amountInString = String.valueOf(amount);
			}
			// second column (amount)
			pg = new Paragraph(amountInString);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setColspan(1);
			table.addCell(cellItem);

			// third column (price)
			String priceInString = "";
			if (price != 0) {
				priceInString = String.format("%8.2f", price);
			}
			pg = new Paragraph(priceInString);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setColspan(1);
			table.addCell(cellItem);

			// fourth column (sub-total)
			String totalInString = "";
			if (totalPrice != 0) {
				totalInString = String.format("€%12.2f", totalPrice);
			} else if (totalPrice == 0) {
				totalInString = String.format("€%16s", "-");
			}

			pg = new Paragraph(totalInString);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setColspan(1);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEighteenthRow(PdfPTable table, String firstCol, String secondCol, String thirdCol,
			String fourthCol) {
		try {
			Paragraph pg = new Paragraph(firstCol);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.BOLD, BaseColor.BLACK));
			;
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setColspan(4);
			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph(secondCol);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.BOLD, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(1);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph(thirdCol);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.BOLD, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph(fourthCol);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.BOLD, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.addElement(pg);

			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.BOLD, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			// cellItem.addElement(pg);

			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			// table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			// table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setSixteenthRow(PdfPTable table, String currentDate, String invoiceNumber, String newDate) {
		try {
			Paragraph pg = new Paragraph(currentDate);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.NORMAL, BaseColor.BLACK));
			;
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(2);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_CENTER);
			table.addCell(cellItem);

			pg = new Paragraph(invoiceNumber);
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.NORMAL, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			cellItem.addElement(pg);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(2);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph(newDate);
			pg.setFont(FontFactory.getFont("Arial", 7, Font.NORMAL, BaseColor.BLACK));
			;
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell();
			cellItem.addElement(pg);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(2);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setFifteenthRow(PdfPTable table) {
		try {
			Paragraph pg = new Paragraph("FACTUURDATUM");
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(2);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("FACTUURNUMMER");
			pg.setAlignment(Element.ALIGN_CENTER);
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(2);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("VERVALDATUM");
			pg.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			;
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell();
			cellItem.addElement(pg);

			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(2);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setThirteenthRow(PdfPTable table) {
		try {
			PdfPCell cellItem = new PdfPCell(new Phrase(" "));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			table.addCell(cellItem);
			table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph(" ");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setTwelthRow(PdfPTable table) {
		try {
			Paragraph pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			PdfPCell cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);

			Paragraph phrase = new Paragraph("E-mail:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			phrase.setAlignment(Element.ALIGN_LEFT);
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setPaddingLeft(10f);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("info@hugohopper.nl");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEleventhRow(PdfPTable table) {
		try {
			Paragraph pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			PdfPCell cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);

			Paragraph phrase = new Paragraph("Website:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			phrase.setAlignment(Element.ALIGN_LEFT);
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setPaddingLeft(10f);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("https://www.hugohopper.nl");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setTenthRow(PdfPTable table) {
		try {
			Paragraph pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			PdfPCell cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);

			Paragraph phrase = new Paragraph("IBAN:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			phrase.setAlignment(Element.ALIGN_LEFT);
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setPaddingLeft(10f);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("NL59 RABO 0122527356");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setBankExtraRow(PdfPTable table) {
		try {
			Paragraph pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			PdfPCell cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);

			Paragraph phrase = new Paragraph("BIC:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			phrase.setAlignment(Element.ALIGN_LEFT);
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setPaddingLeft(10f);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("RABONL2U");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setNinthRow(PdfPTable table) {
		try {
			Paragraph pg = new Paragraph();
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);

			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			Paragraph phrase = new Paragraph("Bank:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			phrase.setAlignment(Element.ALIGN_LEFT);
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setPaddingLeft(10f);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("RABOBANK");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEighthRow(PdfPTable table) {
		try {
			Paragraph pg = new Paragraph();
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			;
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// table.addCell(cellItem);

			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			Paragraph phrase = new Paragraph("");
			phrase.setFont(FontFactory.getFont(FontFactory.COURIER, 8, BaseColor.BLACK));
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("");
			phrase.setFont(FontFactory.getFont(FontFactory.COURIER, 8, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setSeventhRow(PdfPTable table) {
		try {
			Paragraph pg = new Paragraph();
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			pg.setAlignment(Element.ALIGN_LEFT);
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// table.addCell(cellItem);

			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			Paragraph phrase = new Paragraph("");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setSixthRow(PdfPTable table, String text) {
		try {
			Paragraph pg = new Paragraph("          " + text);
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// table.addCell(cellItem);

			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			Paragraph phrase = new Paragraph("KvK nr:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			phrase.setAlignment(Element.ALIGN_LEFT);
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setPaddingLeft(10f);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("37163874");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setFifthRow(PdfPTable table, String text) {
		try {
			Paragraph pg = new Paragraph("          " + text);
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// table.addCell(cellItem);

			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			Paragraph phrase = new Paragraph("Tel:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			phrase.setAlignment(Element.ALIGN_LEFT);
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setPaddingLeft(10f);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("072 737 02 17");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEmptyRowBordered(PdfPTable table) {
		try {
			PdfPCell cellItem = new PdfPCell(new Phrase(" "));
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setBorder(Rectangle.LEFT);
			table.addCell(cellItem);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			cellItem.setBorder(Rectangle.RIGHT);
			table.addCell(cellItem);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			table.addCell(cellItem);
			table.addCell(cellItem);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setFourthRow(PdfPTable table, String text) {
		try {
			Paragraph pg = new Paragraph("          " + text);
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph("1703 SC Heerhugowaard");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setThirdRow(PdfPTable table, String text, String kvk) {
		try {
			Paragraph pg = new Paragraph("  " + text);
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph("KvK nr:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph(kvk);

			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(phrase);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setColspan(3);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setTAVRow(PdfPTable table, String text, String btw) {
		try {
			Paragraph pg = new Paragraph("  t.a.v  " + text);
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph("BTW nr:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph(btw);

			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(phrase);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setColspan(3);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setColumnDRow(PdfPTable table, String text) {
		try {
			Paragraph pg = new Paragraph("  " + text);
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph("");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph("");

			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(phrase);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setColspan(3);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setColumnEFRow(PdfPTable table, String zip, String city, String bank) {
		try {
			Paragraph pg = new Paragraph("  " + zip + " " + city);
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph("Bank:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph(bank);

			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(phrase);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setColspan(3);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setIBANRow(PdfPTable table, String country, String iban) {
		try {
			Paragraph pg = new Paragraph("  " + country);
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph("IBAN:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph(iban);

			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(phrase);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setColspan(3);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setVATRow(PdfPTable table, String vat, String bic) {
		try {
			Paragraph pg = new Paragraph("  " + vat);
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph("BIC:");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph(bic);

			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(phrase);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setColspan(3);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEmptyBorderRow(PdfPTable table, String title, String tel) {
		try {
			Paragraph pg = new Paragraph("  ");
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph(title);
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph(tel);

			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(phrase);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setColspan(3);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setClosingBox(PdfPTable table, String tagline) {
		try {
			Paragraph pg = new Paragraph(" ");
			pg.setAlignment(Element.ALIGN_LEFT);
			pg.setFont(FontFactory.getFont("Arial", 10, Font.NORMAL, BaseColor.BLACK));
			PdfPCell cellItem = new PdfPCell();
			cellItem.addElement(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// cellItem.setBorder(Rectangle.RIGHT);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
			// cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			// cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph("");
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			;

			cellItem = new PdfPCell();
			phrase.setAlignment(Element.ALIGN_RIGHT);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(1);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			phrase = new Paragraph(tagline);

			phrase.setFont(FontFactory.getFont("Arial", 6, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.addElement(phrase);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setColspan(3);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setSecondRow(PdfPTable table, String address) {
		try {
			Paragraph pg = new Paragraph("AAN");
			pg.setAlignment(Element.ALIGN_CENTER);
			PdfPCell cellItem = new PdfPCell(pg);

			cellItem.setBackgroundColor(new BaseColor(232, 234, 234, 128));
			cellItem.setColspan(3);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellItem.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cellItem);

			pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);

			Paragraph phrase = new Paragraph(address);
			// phrase.setAlignment(Element.ALIGN_MIDDLE);
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(3);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			/*
			 * phrase = new Phrase(""); cellItem = new PdfPCell(phrase);
			 * cellItem.setBorder(Rectangle.NO_BORDER); table.addCell(cellItem);
			 * table.addCell(cellItem); table.addCell(cellItem);
			 * 
			 * phrase = new Phrase("Belbusvereniging Noordkop");
			 * phrase.setFont(FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD,
			 * BaseColor.BLACK)); cellItem = new PdfPCell(phrase); cellItem = new
			 * PdfPCell(new Phrase("")); cellItem.setBorder(Rectangle.NO_BORDER);
			 * table.addCell(cellItem); table.addCell(cellItem); table.addCell(cellItem);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setFirstRow(PdfPTable table, String imagePath) {
		try {
			Paragraph phrase = new Paragraph("");
			phrase.setFont(FontFactory.getFont("Arial", 12, Font.BOLD, BaseColor.BLACK));

			PdfPCell cellItem = new PdfPCell();
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.setPaddingBottom(10f);
			cellItem.addElement(phrase);

			cellItem.setColspan(3);
			// cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			// cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setBorder(Rectangle.NO_BORDER);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);
			// JOptionPane.showMessageDialog(null,
			// getClass().getResource(IMAGE_NAME).toString());
			// Path path = Paths.get(new URI(IMAGE_NAME));

			Image img = Image.getInstance(imagePath);

			cellItem = new PdfPCell(img);
			cellItem.setColspan(3);
			cellItem.setFixedHeight(100);
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
			// JOptionPane.showMessageDialog(null, e.getMessage());
			PdfPCell cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setColspan(3);
			table.addCell(cellItem);

			cellItem = new PdfPCell(new Phrase(""));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
		}
	}

	private void setFacturerRow(PdfPTable table, String companyName) {
		try {
			Paragraph phrase = new Paragraph("Factuur");
			phrase.setFont(FontFactory.getFont("Arial", 14, Font.BOLD, BaseColor.BLACK));

			PdfPCell cellItem = new PdfPCell();
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);

			cellItem.setColspan(3);
			// cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			Paragraph pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);

			phrase = new Paragraph(companyName);
			// phrase.setAlignment(Element.ALIGN_MIDDLE);
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.BOLD, BaseColor.BLACK));
			cellItem = new PdfPCell();
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(3);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			/*
			 * phrase = new Phrase(""); cellItem = new PdfPCell(phrase);
			 * cellItem.setBorder(Rectangle.NO_BORDER); table.addCell(cellItem);
			 * table.addCell(cellItem); table.addCell(cellItem);
			 * 
			 * phrase = new Phrase("Belbusvereniging Noordkop");
			 * phrase.setFont(FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD,
			 * BaseColor.BLACK)); cellItem = new PdfPCell(phrase); cellItem = new
			 * PdfPCell(new Phrase("")); cellItem.setBorder(Rectangle.NO_BORDER);
			 * table.addCell(cellItem); table.addCell(cellItem); table.addCell(cellItem);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setCountryRow(PdfPTable table, String country) {
		try {
			Paragraph phrase = new Paragraph("");
			phrase.setFont(FontFactory.getFont("Arial", 14, Font.BOLD, BaseColor.BLACK));

			PdfPCell cellItem = new PdfPCell();
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);

			cellItem.addElement(phrase);

			cellItem.setColspan(3);
			// cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			Paragraph pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setPaddingBottom(10f);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);

			phrase = new Paragraph(country);
			// phrase.setAlignment(Element.ALIGN_MIDDLE);
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();

			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(3);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			/*
			 * phrase = new Phrase(""); cellItem = new PdfPCell(phrase);
			 * cellItem.setBorder(Rectangle.NO_BORDER); table.addCell(cellItem);
			 * table.addCell(cellItem); table.addCell(cellItem);
			 * 
			 * phrase = new Phrase("Belbusvereniging Noordkop");
			 * phrase.setFont(FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD,
			 * BaseColor.BLACK)); cellItem = new PdfPCell(phrase); cellItem = new
			 * PdfPCell(new Phrase("")); cellItem.setBorder(Rectangle.NO_BORDER);
			 * table.addCell(cellItem); table.addCell(cellItem); table.addCell(cellItem);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setCountryRow2(PdfPTable table, String country) {
		try {
			Paragraph phrase = new Paragraph("");
			phrase.setFont(FontFactory.getFont("Arial", 14, Font.BOLD, BaseColor.BLACK));

			PdfPCell cellItem = new PdfPCell();
			cellItem.setBorder(Rectangle.NO_BORDER);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			// cellItem.setPaddingBottom(10f);
			cellItem.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
			cellItem.addElement(phrase);

			cellItem.setColspan(3);
			// cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			Paragraph pg = new Paragraph("");
			pg.setAlignment(Element.ALIGN_CENTER);
			cellItem = new PdfPCell(pg);
			cellItem.setPaddingBottom(10f);
			cellItem.setColspan(2);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			// table.addCell(cellItem);
			// table.addCell(cellItem);

			phrase = new Paragraph(country);
			// phrase.setAlignment(Element.ALIGN_MIDDLE);
			phrase.setFont(FontFactory.getFont("Arial", 8, Font.NORMAL, BaseColor.BLACK));
			cellItem = new PdfPCell();
			// cellItem.setPaddingBottom(10f);
			cellItem.setVerticalAlignment(Element.ALIGN_BOTTOM);
			cellItem.addElement(phrase);
			cellItem.setColspan(3);
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);

			/*
			 * phrase = new Phrase(""); cellItem = new PdfPCell(phrase);
			 * cellItem.setBorder(Rectangle.NO_BORDER); table.addCell(cellItem);
			 * table.addCell(cellItem); table.addCell(cellItem);
			 * 
			 * phrase = new Phrase("Belbusvereniging Noordkop");
			 * phrase.setFont(FontFactory.getFont(FontFactory.COURIER, 12, Font.BOLD,
			 * BaseColor.BLACK)); cellItem = new PdfPCell(phrase); cellItem = new
			 * PdfPCell(new Phrase("")); cellItem.setBorder(Rectangle.NO_BORDER);
			 * table.addCell(cellItem); table.addCell(cellItem); table.addCell(cellItem);
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEmptyRow(PdfPTable table) {
		try {
			PdfPCell cellItem = new PdfPCell(new Phrase(" "));
			cellItem.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellItem);
			table.addCell(cellItem);
			table.addCell(cellItem);
			table.addCell(cellItem);
			table.addCell(cellItem);
			table.addCell(cellItem);
			table.addCell(cellItem);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
