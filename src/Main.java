import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JOptionPane;
import model.Company;
import model.Database;
import model.EmailServer;
import model.Utils;

public class Main {

	public static void main(String[] args) {

		try {
			File file = new File(Utils.INVOICES_PATH);
			if (!file.exists()) {
				file.mkdir();
			}

			file = new File(Utils.LOGO_PATH);
			if (!file.exists()) {
				file.mkdir();
			}

			if (Database.getInstance().getCompanyDetails(Utils.COMPANY_ID) == null) {
				Database.getInstance().insertCompany(new Company(), Utils.COMPANY_ID);
				Database.getInstance().insertCustomerIdCounter(Utils.CUSTOMER_COUNTER_ID, 1001);
				EmailServer emailServer = new EmailServer();
				emailServer.SetUsername("216e40001@smtp-brevo.com");
				emailServer.SetPassword("xsmtpsib-ccc8b7a854e3988e34f71f6ca94bddea7ce3df003264ad874874b2005cd65599-m2sLjDPYI0OJcHQa");
				Database.getInstance().insertServerSMTPDetails(emailServer);
			}
			
			
		//	System.out.println(Database.getInstance().getLastInvoiceNumber());
			/*
			 * try { for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) { if
			 * ("Nimbus".equals(info.getName())) {
			 * UIManager.setLookAndFeel(info.getClassName()); break; } } } catch (Exception
			 * e) { // If Nimbus is not available, you can set the GUI to another look and
			 * feel. }
			 */

			// Database.getInstance().getCurrentMonthTotal();
			// System.out.println(Database.getInstance().getCurrentMonthYearly());

			System.out.println(Database.getInstance().getNumberOfItemsThisYear());
			//System.out.println(System.getProperty("user.dir") + Utils.INVOICES_PATH);
			new Application();
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace = sw.toString();
			JOptionPane.showMessageDialog(null, sStackTrace);
		}
	}
}
