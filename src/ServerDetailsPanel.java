import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import model.Database;
import model.EmailServer;

public class ServerDetailsPanel extends JPanel{

	
	private JTextField usernameField;
	private JPasswordField passwordField;
	private Database database;
	EmailServer details;
	public ServerDetailsPanel() {
		
		
		database = Database.getInstance();
		details = database.getEmailServerDetails();
		setLayout(new GridBagLayout());
		GridLayout gridLayout = new GridLayout(3, 2, 10, 10); 
		
		setBorder(new EmptyBorder(50, 50, 50, 50));
		
		
		JPanel mainLayout = new JPanel(gridLayout);
		TitledBorder border = BorderFactory.createTitledBorder("Email server details");
		mainLayout.setBorder(new CompoundBorder(border, new EmptyBorder(20, 20, 20, 20)));
		//first row
		mainLayout.add(new JLabel("Username"));
		usernameField = new JTextField(60);
		mainLayout.add(usernameField);
		
		
		//second row
		mainLayout.add(new JLabel("Password"));
		passwordField = new JPasswordField(60);
		mainLayout.add(passwordField);
		
		mainLayout.add(new JLabel(""));
		JButton updateButton = new JButton("Update");
		
		
		if(details != null) {
			usernameField.setText(details.GetUsername());
			passwordField.setText(details.GetPassword());
			
		}
		updateButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(details == null) {
					details = new EmailServer();
					details.SetUsername(usernameField.getText());
					details.SetPassword(new String(passwordField.getPassword()));
					System.out.println(details.GetPassword());
					database.insertServerSMTPDetails(details);
				}
				else {
					details.SetUsername(usernameField.getText());
					details.SetPassword(new String(passwordField.getPassword()));
					System.out.println(details.GetPassword());
					database.setEmailServerDetails(details);
				}
				
			}
			
		});
		
		mainLayout.add(updateButton);
		
		add(mainLayout);
		
	}
}
