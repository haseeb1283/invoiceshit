/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

import model.Bean;
import model.Company;
import model.Database;
import model.EmailServer;
import model.Utils;

/**
 *
 * @author Haseeb
 */
public class SendEmail {
    
    private static final String HOST_NAME = "smtp-relay.brevo.com";
    private static String SENDER = "bart.verver@verleijtradingeurope.nl";
    private static String PASSWORD = "FUpOkRXNAjP97D8Q";
    private static final String SUBJECT = "Factuur van geleverde diensten of producten";
    private static final String SUBJECT_ANNUAL = "Factuur van uw jaarlijkse lidmaatschapskosten van de HugoHopper";
    //private static String to = "haseeb1283@gmail.com";//change accordingly

    //private static final String SENDER_ALIAS = "info@hugohopper.nl";
   // private static final String SENDER_ALIAS_NAME = "Administratie HugoHopper";
    private static final String BCC_EMAIL = "invoice@verleijtradingeurope.nl";
    
    private static String allEmailsSent = "";
    private static Database database;
    
    public static boolean sendMail(Bean bean, Company company) {

    	database = Database.getInstance();
    	
    	EmailServer server = database.getEmailServerDetails();
    	SENDER = server.GetUsername();
    	PASSWORD = server.GetPassword();
    	
    	
        //Get the session object  
        Properties props = new Properties();
        props.put("mail.smtp.host", HOST_NAME); //SMTP Host
        props.put("mail.smtp.socketFactory.port", "587"); //SSL Port
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
        props.put("mail.smtp.auth", "true"); //Enabling SMTP Authentication
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.starttls.enable", "true");
        
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(SENDER, PASSWORD);
            }
        });
        
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            //msg.addHeader("format", "flowed");
            //msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress(company.getSenderEmail(), company.getName()));
            msg.setReplyTo(InternetAddress.parse(company.getSenderEmail(), false));
            msg.setSubject(SUBJECT, "UTF-8");
           
            
            msg.setSentDate(new Date());
            String emailsArray = bean.getEmailAddress();
            String[] partsEmails = emailsArray.split(";");
            for(String email : partsEmails) {
            	msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            }
          //  msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(bean.getEmailAddress(), false));
            msg.addRecipient(RecipientType.BCC, new InternetAddress(BCC_EMAIL));

            // Create the message body part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setContent(getHtmlFormatString(company), "text/html; charset=UTF-8");
            
            // Create a multipart message for attachment
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Second part is attachment
            messageBodyPart = new MimeBodyPart();
            
            String path = System.getProperty("user.dir")+File.separator+Utils.INVOICES_PATH+"INVOICE_"+bean.getInvoiceNumber()+".pdf";
            System.out.println(path);
            DataSource source = new FileDataSource(path);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName("INVOICE_"+bean.getInvoiceNumber()+".pdf");
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            msg.setContent(multipart);
            msg.saveChanges();
            // Send message
            Transport.send(msg);
            bean.setInvoiceSent(true);
            return true;
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            return false;
        }
    }
   
    private static String getAnnualHtmlFormatString(Company company) {
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<meta name=\"viewport\" content=\"width=device-width\">\n"
                + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
                + "<title>Simple Transactional Email</title>\n"
                + "</head>\n"
                + "<body class=\"\" style=\"background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;\">\n"
                + "    <table style=\"border-collapse: separat; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;\" role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\"><tr>\n"
                + "<td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;\"> </td>\n"
                + "        <td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;display:block;margin:0 auto !important;max-width:580px;padding:0 !important;width:100% !important;\" class=\"container\">\n"
                + "          <div style=\"box-sizing:border-box;display:block;margin:0 auto;max-width:580px;padding:0 !important;\" class=\"content\">\n"
                + " \n"
                + "            <!-- START CENTERED WHITE CONTAINER -->\n"
                + " \n"
                + "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px; border-collapse: separat; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;\"><tr>\n"
                + "<td align=\"center\" valign=\"top\" style=\"padding:40px 10px 40px 10px;font-family:sans-serif;font-size:16px !important;vertical-align:top;\">\n"
                + "                        <a href=\"https://www.hugohopper.nl\" target=\"_blank\" style=\"color:#3498db;text-decoration:underline;font-size:16px !important;\">\n"
                + "                            <img style=\"background-color:white;border: none; -ms-interpolation-mode: bicubic; max-width: 100%; width: 80%; height: 80%; border: 0;\" alt=\"Logo\" src=\"https://www.hugohopper.nl/wp-content/uploads/2018/07/Logo_Hugohopper_witte_outline2.png\"></a>\n"
                + "                    </td>\n"
                + "                </tr></table>\n"
                + "<table style=\"border-collapse:separat;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;border-radius:0 !important;width:100%;border-left-width:0 !important;border-right-width:0 !important;\" role=\"presentation\" class=\"main\">\n"
                + "<!-- START MAIN CONTENT AREA --><tr>\n"
                + "<td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;box-sizing:border-box;padding:10px !important;\" class=\"wrapper\">\n"
                + "                  <table style=\"border-collapse: separat; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;\" role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr>\n"
                + "<td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;\">\n"
                + "                          <p style=\"font-family:sans-serif;font-size:16px !important;font-weight:normal;margin:0;margin-bottom:15px;\"><br><br>Geachte heer/mevrouw,</p>\n"
                + "                          <p style=\"font-family:sans-serif;font-size:16px !important;font-weight:normal;margin:0;margin-bottom:15px;\">\n"
                + "                            In de bijlage van deze mail kunt u de factuur van uw jaarlijkse lidmaatschapskosten vinden. Deze PDF factuur kunt u openen met Acrobat Reader, mocht dit nog niet op uw computer geïnstalleerd staan."
                + "\n"
                + "                            <br><br>Hartelijk dank voor uw vertrouwen in de HugoHopper en graag tot een volgende keer!\n"
                + "                            <br><br>Met vriendelijke groet,\n"
                + "                            <br><br>HugoHopper/Vervoersvereniging Heerhugowaard\n"
                + "                          </p>\n"
                + "                        </td>\n"
                + "                      </tr></tbody></table>\n"
                + "</td>\n"
                + "              </tr>\n"
                + "<!-- END MAIN CONTENT AREA -->\n"
                + "</table>\n"
                + "<!-- START FOOTER --><div style=\"clear: both; Margin-top: 10px; text-align: center; width: 100%;\" class=\"footer\">\n"
                + "              <table style=\"border-collapse: separat; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;\" role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>\n"
                + "<td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;padding-bottom:10px;padding-top:10px;color:#999999;text-align:center;\" class=\"content-block\">\n"
                + "                    <span style=\"color:#999999;font-size:16px !important;text-align:center;\" class=\"apple-link\">HugoHopper <br> Middenwaard 127<br>1703SC Heerhugowaard<br>Tel: 072 737 02 17</span>\n"
                + "                  </td>\n"
                + "                </tr></table>\n"
                + "</div>\n"
                + "            <!-- END FOOTER -->\n"
                + " \n"
                + "          <!-- END CENTERED WHITE CONTAINER -->\n"
                + "          </div>\n"
                + "        </td>\n"
                + "        <td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;\"> </td>\n"
                + "      </tr></table>\n"
                + "</body>\n"
                + "</html>";
    }
    
    private static String getHtmlFormatString(Company company) {
        String string = "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<meta name=\"viewport\" content=\"width=device-width\">\n"
                + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
                + "<title>Simple Transactional Email</title>\n"
                + "</head>\n"
                + "<body class=\"\" style=\"background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;\">\n"
                + "    <table style=\"border-collapse: separat; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;\" role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\"><tr>\n"
                + "<td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;\"> </td>\n"
                + "        <td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;display:block;margin:0 auto !important;max-width:580px;padding:0 !important;width:100% !important;\" class=\"container\">\n"
                + "          <div style=\"box-sizing:border-box;display:block;margin:0 auto;max-width:580px;padding:0 !important;\" class=\"content\">\n"
                + " \n"
                + "            <!-- START CENTERED WHITE CONTAINER -->\n"
                + " \n"
                + "            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px; border-collapse: separat; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;\"><tr>\n"
                + "<td align=\"center\" valign=\"top\" style=\"padding:40px 10px 40px 10px;font-family:sans-serif;font-size:16px !important;vertical-align:top;\">\n"
                + "                        <a href=\"https://www.hugohopper.nl\" target=\"_blank\" style=\"color:#3498db;text-decoration:underline;font-size:16px !important;\">\n"
                + "                    </td>\n"
                + "                </tr></table>\n"
                + "<table style=\"border-collapse:separat;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;border-radius:0 !important;width:100%;border-left-width:0 !important;border-right-width:0 !important;\" role=\"presentation\" class=\"main\">\n"
                + "<!-- START MAIN CONTENT AREA --><tr>\n"
                + "<td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;box-sizing:border-box;padding:10px !important;\" class=\"wrapper\">\n"
                + "                  <table style=\"border-collapse: separat; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;\" role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tbody><tr>\n"
                + "<td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;\">\n"
                + "                          <p style=\"font-family:sans-serif;font-size:16px !important;font-weight:normal;margin:0;margin-bottom:15px;\"><br><br>Beste klant,</p>\n"
                + "                          <p style=\"font-family:sans-serif;font-size:16px !important;font-weight:normal;margin:0;margin-bottom:15px;\">\n"
                + "                            Hartelijk dank voor uw opdracht of bestelling, welke reeds uitgevoerd of\r\n"
                + "geleverd is. Zie bijlage voor de factuur.\n"
                + "                          </p>\n"
                + "                          <p style=\"font-family:sans-serif;font-size:16px !important;font-weight:normal;margin:0;margin-bottom:15px;\">\n"
                + "                            Met vriendelijke groet,\n"
                + "                          </p>\n"
                + "                          <p style=\"font-family:sans-serif;font-size:16px !important;font-weight:normal;margin:0;margin-bottom:15px;\">\n"
                + "                            Bart Verver\n"
                + "                          </p>\n"
                + "                        </td>\n"
                + "                      </tr></tbody></table>\n"
                + "</td>\n"
                + "              </tr>\n"
                + "<!-- END MAIN CONTENT AREA -->\n"
                + "</table>\n"
                + "<!-- START FOOTER --><div style=\"clear: both; Margin-top: 10px; text-align: center; width: 100%;\" class=\"footer\">\n"
                + "              <table style=\"border-collapse: separat; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;\" role=\"presentation\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>\n"
                + "<td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;padding-bottom:10px;padding-top:10px;color:#999999;text-align:center;\" class=\"content-block\">\n"
                + "                    <span style=\"color:#999999;font-size:16px !important;text-align:center;\" class=\"apple-link\">"+company.getName()+" <br> " + company.getCountry1()+"<br>"+company.getAddress1();
                
        		if(!company.getTel1().trim().isEmpty() && !company.getTel2().trim().isEmpty()) {
        			string +="<br>Tel1: "+company.getTel1();
        			string +="<br>Tel2: "+company.getTel2()+"</span>\n";
        		}
        		else {
	        		if(!company.getTel1().trim().isEmpty())						
	                	string +="<br>Tel1: "+company.getTel1()+"</span>\n";
	                
	        		else if(!company.getTel2().trim().isEmpty())
	                	string +="<br>Tel2: "+company.getTel2()+"</span>\n";
        		}
                string += "                  </td>\n"
                + "                </tr></table>\n"
                + "</div>\n"
                + "            <!-- END FOOTER -->\n"
                + " \n"
                + "          <!-- END CENTERED WHITE CONTAINER -->\n"
                + "          </div>\n"
                + "        </td>\n"
                + "        <td style=\"font-family:sans-serif;font-size:16px !important;vertical-align:top;\"> </td>\n"
                + "      </tr></table>\n"
                + "</body>\n"
                + "</html>";
        return string;
    }
}
