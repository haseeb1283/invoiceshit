package model;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

public class Utils {
	public static final String INVOICES_PATH = "INVOICES"+File.separator;
	public static final String LOGO_PATH = "LOGO"+File.separator;
	public static final int COMPANY_ID = 1;
	public static final String APP_ID = "KSJDFJ38O9483_lkj";
	public static final int CUSTOMER_COUNTER_ID = 1;

	public static String getTodayDate() {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			return format.format(new Date());
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Date getDateFullFormatDate(String date) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			return format.parse(date);
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public static Date getDateInSimpleFormat(String date) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			return format.parse(date);
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getDisplayDate(String date) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date newDate = format.parse(date);
			format = new SimpleDateFormat("dd-MM-yyyy");
			return format.format(newDate);
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getDisplayDateInMonthsYear(String date) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date newDate = format.parse(date);
			format = new SimpleDateFormat("yyyy/MM");
			return format.format(newDate);
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String addDates(String invoiceDate, int i) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			Date date = format.parse(invoiceDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.DATE, cal.get(Calendar.DATE) + i);
			return format.format(cal.getTime());
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getDatabaseDate(String date){
		
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			Date newDate;
			try {
				newDate = format.parse(date);
				format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				return format.format(newDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
	}
}
