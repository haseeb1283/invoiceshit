package model;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Database {
	private Connection conn = null;
	private final String DATABASE_NAME = "database.db";
	private Statement stmt = null;

	public static Database instance;

	// table invoice
	public static final String TABLE_INVOICE = "INVOICES";
	public static final String COL_CUSTOMERNO = "CUSTOMER_NO";
	public static final String COL_INVOICENO = "INVOICE_NO";
	public static final String COL_COMPANYNAME = "COMPANY_NAME";
	public static final String COL_PERSONNAME = "PERSON_NAME";
	public static final String COL_ADDRESS = "ADDRESS";
	public static final String COL_ZIPCODE = "ZIPCODE";
	public static final String COL_CITY = "CITY";
	public static final String COL_COUNTRY = "COUNTRY";
	public static final String COL_VATNUMBER = "VAT_NUMBER";
	public static final String COL_EMAILADDRESS = "EMAIL_ADDRESS";
	public static final String COL_VATPERCENTAGE = "VAT_PERCENTAGE";
	public static final String COL_ITEM1DESCRIPTION = "ITEM_1_DESCRIPTION";
	public static final String COL_ITEM1AMOUNT = "ITEM_1_AMOUNT";
	public static final String COL_ITEM1PRICE = "ITEM_1_PRICE";
	public static final String COL_INVOICEDATE = "INVOICE_DATE";
	public static final String COL_ITEM2DESCRIPTION = "ITEM_2_DESCRIPTION";
	public static final String COL_ITEM2AMOUNT = "ITEM_2_AMOUNT";
	public static final String COL_ITEM2PRICE = "ITEM_2_PRICE";
	public static final String COL_ITEM3DESCRIPTION = "ITEM_3_DESCRIPTION";
	public static final String COL_ITEM3AMOUNT = "ITEM_3_AMOUNT";
	public static final String COL_ITEM3PRICE = "ITEM_3_PRICE";
	public static final String COL_HASSHIPMENTCOST = "HAS_SHIPMENT_COST";
	public static final String COL_SHIPMENTCOSTNUMBER = "SHIPMENT_COST_NUMBER";
	public static final String COL_SHIPMENTCOST = "SHIPMENT_COST";
	public static final String COL_INVOICESENT = "INVOICE_SENT";
	public static final String COL_CUMULATIVECOST = "CUMULATIVE_COST";
	// private static final String TABLE_INVOICE_VIRTUAL = TABLE_INVOICE + "_TEMP";

	public static final String TABLE_COMPANY = "COMPANY";
	public static final String COL_COMPANY_INVOICE = "COMPANY_INVOICE";
	public static final String COL_PHOTO_PATH = "PHOTO_PATH";
	public static final String COL_COMPANY_NAME = "NAME";
	public static final String COL_STREETNUMBER_NUMBER = "STREETNUMBER_NUMBER";
	public static final String COL_ZIPCODE_CITY = "ZIPCODE_CITY";
	public static final String COL_COMPANY_COUNTRY = "COUNTRY";
	public static final String COL_KVK = "KVK";
	public static final String COL_BTW = "BTW";
	public static final String COL_BANK = "BANK";
	public static final String COL_IBAN = "IBAN";
	public static final String COL_TAGLINE = "TAGLINE";
	public static final String COL_SENDERS_EMAIL = "SENDERS_EMAIL";
	public static final String COL_TEL1 = "TEL1";
	public static final String COL_TEL2 = "TEL2";
	public static final String COL_BIC = "BIC";
	public static final String COL_WEBSITE = "Website";
	public static final String COL_REINIGEN = "Reinigen";

	public static final String TABLE_CUSTOMER_NO = "CUSTOMER_NUMBER_COUNTER";
	public static final String COL_CUSTOMER_NO_ID = "COUNTER_ID";
	public static final String COL_CUSTOMERID_COUNTER = "CUST_ID_COUNTER";
	
	
	//TABLE FOR EMAIL SERVER CREDENTIALS
	public static final String TABLE_EMAIL_SERVER = "EMAIL_SERVER";
	public static final String COL_SERVER_ID = "ID";
	public static final int SERVER_ID = 1001;
	public static final String COL_USERNAME = "USER_NAME";
	public static final String COL_PASSWORD = "PASSWORD";

	public static Database getInstance() {
		if (instance == null) {
			instance = new Database();
		}
		return instance;
	}

	private Database() {
		createNewDatabase();
		createTables();
	}

	public void createTables() {
		// creating user table

		String invoiceTable = "Create Table IF NOT EXISTS " + TABLE_INVOICE + "(" + COL_INVOICENO
				+ " INTEGER PRIMARY KEY NOT NULL," + COL_CUSTOMERNO + " INTEGER UNIQUE NOT NULL," + COL_COMPANYNAME
				+ " TEXT," + COL_PERSONNAME + " TEXT, " + COL_ADDRESS + " TEXT, " + COL_ZIPCODE + " TEXT, " + COL_CITY
				+ " TEXT, " + COL_COUNTRY + " TEXT, " + COL_VATNUMBER + " TEXT, " + COL_EMAILADDRESS + " TEXT, "
				+ COL_VATPERCENTAGE + " REAL, " + COL_ITEM1DESCRIPTION + " TEXT, " + COL_ITEM1AMOUNT + " INTEGER, "
				+ COL_ITEM1PRICE + " REAL, " + COL_INVOICEDATE + " TEXT DEFAULT CURRENT_TIMESTAMP, "
				+ COL_ITEM2DESCRIPTION + " TEXT, " + COL_ITEM2AMOUNT + " INTEGER, " + COL_ITEM2PRICE + " REAL, "
				+ COL_ITEM3DESCRIPTION + " TEXT, " + COL_ITEM3AMOUNT + " INTEGER, " + COL_ITEM3PRICE + " REAL, "
				+ COL_HASSHIPMENTCOST + " TEXT, " + COL_SHIPMENTCOSTNUMBER + " TEXT, " + COL_SHIPMENTCOST + " REAL, "
				+ COL_INVOICESENT + " TEXT, " + COL_CUMULATIVECOST + " REAL" + ");";
		createTable(invoiceTable);

		String custerIdCounterTable = "Create Table IF NOT EXISTS " + TABLE_CUSTOMER_NO + " (" + COL_CUSTOMER_NO_ID
				+ " INTEGER PRIMARY KEY NOT NULL, " + COL_CUSTOMERID_COUNTER + " INTEGER NOT NULL);";
		System.out.println(custerIdCounterTable);
		createTable(custerIdCounterTable);

		String companyTable = "Create Table if NOT EXISTS " + TABLE_COMPANY + " (" + COL_COMPANY_INVOICE
				+ " INTEGER NOT NULL," + COL_PHOTO_PATH + " BLOB," + COL_TAGLINE + " TEXT," + COL_SENDERS_EMAIL
				+ " TEXT," + COL_TEL1 + " TEXT," + COL_TEL2 + " TEXT," + COL_COMPANY_NAME + " TEXT,"
				+ COL_STREETNUMBER_NUMBER + " TEXT," + COL_COMPANY_COUNTRY + " TEXT," + COL_ZIPCODE_CITY + " TEXT, "
				+ COL_KVK + " TEXT, " + COL_BTW + " TEXT, " + COL_BANK + " TEXT, " + COL_IBAN + " TEXT, " + COL_WEBSITE
				+ " TEXT, " + COL_REINIGEN + " TEXT, " + COL_BIC + " TEXT);";
		System.out.println(companyTable);
		createTable(companyTable);
		
		
		
		//EMAIL SERVER DATABASE
		
		String serverTable = "Create Table if NOT EXISTS " + TABLE_EMAIL_SERVER + " ( " + COL_SERVER_ID +" INTEGER PRIMARY KEY NOT NULL," + COL_USERNAME + " TEXT NOT NULL," +
		COL_PASSWORD + " TEXT NOT NULL);";
		System.out.println(serverTable);
		createTable(serverTable);
		
		/*
		 * String virtualTable =
		 * "CREATE VIRTUAL TABLE IF NOT EXISTS "+TABLE_INVOICE_VIRTUAL+" USING fts5("+
		 * COL_INVOICENO+"," +COL_COMPANYNAME+"," +COL_PERSONNAME+"," +COL_CITY+","
		 * +COL_ZIPCODE+"," +COL_ADDRESS+");"; createTable(virtualTable);
		 */
	}

	public void createTable(String query) {
		try {
			stmt = conn.createStatement();

			stmt.executeUpdate(query);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	public void createNewDatabase() {

		String url = "jdbc:sqlite:" + DATABASE_NAME;

		try {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection(url);

			if (conn != null) {
				DatabaseMetaData meta = conn.getMetaData();
				turnOnForeignKeys();
				System.out.println("The driver name is " + meta.getDriverName());
				System.out.println("A new database has been created.");
			} else {
				System.out.println("null");
			}

		} catch (SQLException | ClassNotFoundException e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace = sw.toString();
			JOptionPane.showMessageDialog(null, sStackTrace);
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
	}

	private void turnOnForeignKeys() {
		try {
			stmt = conn.createStatement();
			String query = "PRAGMA foreign_keys = ON;";
			stmt.executeUpdate(query);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void insertServerSMTPDetails(EmailServer emailServerDetails) {
		try {
			stmt = conn.createStatement();
			String sql = "INSERT INTO " + TABLE_EMAIL_SERVER + "(" + COL_SERVER_ID+", " + COL_USERNAME + "," + COL_PASSWORD +") VALUES (" +
					SERVER_ID +",'"+ emailServerDetails.GetUsername() +"', '" + emailServerDetails.GetPassword()+"');";
			System.out.println(sql);
			stmt.executeUpdate(sql);
			stmt.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public EmailServer getEmailServerDetails() {
		try {
			stmt = conn.createStatement();
			String sql = "SELECT * FROM " + TABLE_EMAIL_SERVER +" WHERE " + COL_SERVER_ID + "="+  SERVER_ID;
			
			ResultSet rs = stmt.executeQuery(sql);
			EmailServer server = null;
			if(rs.next()) {
				server = new EmailServer();
				server.SetUsername(rs.getString(COL_USERNAME));
				server.SetPassword(rs.getString(COL_PASSWORD));
			}
			stmt.close();
			return server;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean setEmailServerDetails(EmailServer emailServer) {
		try {
			stmt = conn.createStatement();
			
			String sql = "UPDATE " + TABLE_EMAIL_SERVER + " SET " + COL_USERNAME + "='"+ emailServer.GetUsername()+"', " + COL_PASSWORD + "='" + emailServer.GetPassword()+"' "
					+ "WHERE " + COL_SERVER_ID +"="+ SERVER_ID+";";
			
			System.out.println(sql);
			boolean result = stmt.execute(sql);
			stmt.close();
			
			return result;
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean insertBean(Bean bean) {
		try {
			stmt = conn.createStatement();
			String sql = "INSERT INTO " + TABLE_INVOICE + " (" + COL_INVOICENO + "," + COL_CUSTOMERNO + ","
					+ COL_COMPANYNAME + "," + COL_PERSONNAME + "," + COL_ADDRESS + "," + COL_ZIPCODE + "," + COL_CITY
					+ "," + COL_COUNTRY + "," + COL_VATNUMBER + "," + COL_EMAILADDRESS + "," + COL_VATPERCENTAGE + ","
					+ COL_ITEM1DESCRIPTION + "," + COL_ITEM1AMOUNT + "," + COL_ITEM1PRICE + "," + COL_ITEM2DESCRIPTION
					+ "," + COL_ITEM2AMOUNT + "," + COL_ITEM2PRICE + "," + COL_ITEM3DESCRIPTION + "," + COL_ITEM3AMOUNT
					+ "," + COL_ITEM3PRICE + "," + COL_HASSHIPMENTCOST + "," + COL_SHIPMENTCOSTNUMBER + ","
					+ COL_SHIPMENTCOST + "," + COL_INVOICESENT + "," + COL_CUMULATIVECOST + ") " + "VALUES ('"
					+ bean.getInvoiceNumber() + "'," + bean.getCustomerNumber() + ",'" + bean.getCompanyName() + "','"
					+ bean.getPersonName() + "','" + bean.getAddress() + "','" + bean.getZipcode() + "','"
					+ bean.getCity() + "','" + bean.getCountry() + "','" + bean.getVatNumber() + "','"
					+ bean.getEmailAddress() + "'," + bean.getVatPercentage() + ",'" + bean.getItemOneDescription()
					+ "'," + bean.getAmountOfItemsOne() + "," + bean.getPriceItemOne()
					+ /* ",'" + bean.getInvoiceDate() *+ */ ",'" + bean.getItemTwoDescription() + "',"
					+ bean.getAmountOfItemsTwo() + "," + bean.getPriceItemTwo() + ",'" + bean.getItemThreeDescription()
					+ "'," + bean.getAmountOfItemsThree() + "," + bean.getPriceItemThree() + ",'"
					+ bean.isHasShipmentCost() + "'," + bean.getShipmentCostNumber() + "," + bean.getShipmentCost()
					+ ",'" + bean.isInvoiceSent() + "'," + bean.getAllCostCombined() + ");";

			System.out.println(sql);
			stmt.executeUpdate(sql);
			stmt.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public void insertCompany(Company company, int companyId) {
		try {
			stmt = conn.createStatement();
			String sql = "INSERT INTO " + TABLE_COMPANY + " (" + COL_COMPANY_INVOICE + "," + COL_TAGLINE + ","
					+ COL_SENDERS_EMAIL + "," + COL_TEL1 + "," + COL_TEL2 + "," + COL_COMPANY_NAME + ","
					+ COL_STREETNUMBER_NUMBER + "," + COL_ZIPCODE_CITY + "," + COL_COMPANY_COUNTRY + "," + COL_KVK + ","
					+ COL_BTW + "," + COL_BANK + "," + COL_IBAN + "," + COL_WEBSITE + "," + COL_REINIGEN + "," + COL_BIC
					+ ") " + "VALUES (" + companyId + ",'" + company.getTagLine() + "','" + company.getSenderEmail()
					+ "','" + company.getTel1() + "','" + company.getTel2() + "','" + company.getName() + "','"
					+ company.getCountry1() + "','" + company.getAddress1() + "','" + company.getCountry2() + "','"
					+ company.getKvk() + "','" + company.getBtw() + "','" + company.getBank() + "','"
					+ company.getIban() + "','" + company.getWebsite() + "','" + company.getReinigen() + "','"
					+ company.getBic() + "');";
			System.out.println(sql);
			stmt.executeUpdate(sql);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Company getCompanyDetails(int companyId) {
		try {
			stmt = conn.createStatement();
			String query = "SELECT * FROM " + TABLE_COMPANY + " WHERE " + COL_COMPANY_INVOICE + " = " + companyId;
			ResultSet result = stmt.executeQuery(query);

			Company company = null;
			if (result.next()) {
				company = new Company();
				// company.setPhotoPath(result.getString(COL_PHOTO_PATH));
				InputStream input = result.getBinaryStream(COL_PHOTO_PATH);
				if (input != null) {
					File logo = new File(Utils.LOGO_PATH + companyId + ".png");
					FileOutputStream fos = new FileOutputStream(logo);
					byte[] buffer = new byte[1024];
					while (input.read(buffer) > 0) {
						fos.write(buffer);
					}
					company.setPhotoPath(logo);
					fos.close();
				}
				company.setWebsite(result.getString(COL_WEBSITE));
				company.setName(result.getString(COL_COMPANY_NAME));
				company.setCountry1(result.getString(COL_STREETNUMBER_NUMBER));
				company.setCountry2(result.getString(COL_COMPANY_COUNTRY));
				company.setAddress1(result.getString(COL_ZIPCODE_CITY));
				company.setKvk(result.getString(COL_KVK));
				company.setBtw(result.getString(COL_BTW));
				company.setBank(result.getString(COL_BANK));
				company.setBic(result.getString(COL_BIC));
				company.setIban(result.getString(COL_IBAN));
				company.setTagLine(result.getString(COL_TAGLINE));
				company.setTel1(result.getString(COL_TEL1));
				company.setTel2(result.getString(COL_TEL2));
				company.setSenderEmail(result.getString(COL_SENDERS_EMAIL));
				company.setReinigen(result.getString(COL_REINIGEN));
			}
			stmt.close();
			return company;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void updateCompanyDetails(Company company, int companyId) {
		try {
			try {
				// stmt = conn.createStatement();

				byte[] image = null;
				if (company.getPhotoPath() != null) {
					image = readFile(company.getPhotoPath());
				}
				String query = "UPDATE " + TABLE_COMPANY + " SET " + COL_PHOTO_PATH + " = '" + company.getPhotoPath()
						+ "', " + COL_COMPANY_NAME + " = '" + company.getName() + "', " + COL_TAGLINE + " = '"
						+ company.getTagLine() + "', " + COL_SENDERS_EMAIL + " = '" + company.getSenderEmail() + "', "
						+ COL_TEL1 + " = '" + company.getTel1() + "', " + COL_TEL2 + " = '" + company.getTel2() + "', "
						+ COL_STREETNUMBER_NUMBER + " = '" + company.getCountry1() + "', " + COL_ZIPCODE_CITY + " = '"
						+ company.getAddress1() + "', " + COL_COMPANY_COUNTRY + " = '" + company.getCountry2() + "', "
						+ COL_KVK + " = '" + company.getKvk() + "', " + COL_BTW + " = '" + company.getBtw() + "', "
						+ COL_BANK + " = '" + company.getBank() + "', " + COL_IBAN + " = '" + company.getIban() + "', "
						+ COL_BIC + " = '" + company.getBic() + "', " + COL_WEBSITE + " = '" + company.getWebsite()
						+ "', " + COL_REINIGEN + " = '" + company.getReinigen() + "',";

				if (image != null) {
					query += COL_PHOTO_PATH + " = ?";
				}
				query += " WHERE " + COL_COMPANY_INVOICE + " = " + companyId + ";";

				PreparedStatement pm = conn.prepareStatement(query);
				pm.setBytes(1, image);
				System.out.println(query);
				pm.executeUpdate();
				pm.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateRow(int oldInvoiceNumber, Bean bean) {
		try {
			try {
				stmt = conn.createStatement();
				String query = "UPDATE " + TABLE_INVOICE + " SET " + COL_INVOICENO + " = " + bean.getInvoiceNumber()
						+ ", " + COL_COMPANYNAME + " = '" + bean.getCompanyName() + "', " + COL_PERSONNAME + " = '"
						+ bean.getPersonName() + "', " + COL_ADDRESS + " = '" + bean.getAddress() + "', " + COL_ZIPCODE
						+ " = '" + bean.getZipcode() + "', " + COL_CITY + " = '" + bean.getCity() + "', " + COL_COUNTRY
						+ " = '" + bean.getCountry() + "', " + COL_VATNUMBER + " = '" + bean.getVatNumber() + "', "
						+ COL_EMAILADDRESS + " = '" + bean.getEmailAddress() + "', " + COL_INVOICEDATE + " = '"
						+ bean.getInvoiceDate() + "', " + COL_VATPERCENTAGE + " = " + bean.getVatPercentage() + ", "
						+ COL_ITEM1DESCRIPTION + " = '" + bean.getItemOneDescription() + "', " + COL_ITEM1AMOUNT + " = "
						+ bean.getAmountOfItemsOne() + ", " + COL_ITEM1PRICE + " = " + bean.getPriceItemOne() + ", "
						+ COL_ITEM2DESCRIPTION + " = '" + bean.getItemTwoDescription() + "', " + COL_ITEM2AMOUNT + " = "
						+ bean.getAmountOfItemsTwo() + ", " + COL_ITEM2PRICE + " = " + bean.getPriceItemTwo() + ", "
						+ COL_ITEM3DESCRIPTION + " = '" + bean.getItemThreeDescription() + "', " + COL_ITEM3AMOUNT
						+ " = " + bean.getAmountOfItemsThree() + ", " + COL_ITEM3PRICE + " = "
						+ bean.getPriceItemThree() + ", " + COL_HASSHIPMENTCOST + " = '" + bean.isHasShipmentCost()
						+ "', " + COL_SHIPMENTCOSTNUMBER + " = '" + bean.getShipmentCostNumber() + "', "
						+ COL_SHIPMENTCOST + " = " + bean.getShipmentCost() + ", " + COL_INVOICESENT + " = '"
						+ bean.isInvoiceSent() + "', " + COL_CUMULATIVECOST + " = " + bean.getAllCostCombined()
						+ " WHERE " + COL_INVOICENO + " = " + oldInvoiceNumber + ";";

				System.out.println(query);
				stmt.executeUpdate(query);
				stmt.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Bean> getAllData() {
		ArrayList<Bean> data = new ArrayList<>();
		try {
			stmt = conn.createStatement();
			String query = "SELECT * FROM " + TABLE_INVOICE;

			ResultSet result = stmt.executeQuery(query);

			while (result.next()) {
				Bean bean = new Bean();
				bean.setCustomerNumber(result.getInt(COL_CUSTOMERNO));
				bean.setInvoiceNumber(result.getInt(COL_INVOICENO));
				if(bean.getCustomerNumber() == 0) {
					bean.setCustomerNumber(getLastCustomerNumber(Utils.CUSTOMER_COUNTER_ID)+1);
					updateCustomerNumber(bean.getInvoiceNumber(), bean.getCustomerNumber());
					updateCustomerIdCounter(Utils.CUSTOMER_COUNTER_ID, bean.getCustomerNumber());
				}			
				bean.setCompanyName(result.getString(COL_COMPANYNAME));
				bean.setPersonName(result.getString(COL_PERSONNAME));
				bean.setCity(result.getString(COL_CITY));
				bean.setZipcode(result.getString(COL_ZIPCODE));
				bean.setAddress(result.getString(COL_ADDRESS));
				bean.setCountry(result.getString(COL_COUNTRY));
				bean.setVatNumber(result.getString(COL_VATNUMBER));
				bean.setEmailAddress(result.getString(COL_EMAILADDRESS));
				bean.setVatPercentage(result.getDouble(COL_VATPERCENTAGE));
				bean.setItemOneDescription(result.getString(COL_ITEM1DESCRIPTION));
				bean.setAmountOfItemsOne(result.getInt(COL_ITEM1AMOUNT));
				bean.setPriceItemOne(result.getDouble(COL_ITEM1PRICE));
				bean.setInvoiceDate(result.getString(COL_INVOICEDATE));
				bean.setItemTwoDescription(result.getString(COL_ITEM2DESCRIPTION));
				bean.setAmountOfItemsTwo(result.getInt(COL_ITEM2AMOUNT));
				bean.setPriceItemTwo(result.getDouble(COL_ITEM2PRICE));
				bean.setItemThreeDescription(result.getString(COL_ITEM3DESCRIPTION));
				bean.setAmountOfItemsThree(result.getInt(COL_ITEM3AMOUNT));
				bean.setPriceItemThree(result.getDouble(COL_ITEM3PRICE));
				bean.setHasShipmentCost(Boolean.valueOf(result.getString(COL_HASSHIPMENTCOST)));
				bean.setShipmentCostNumber(result.getString(COL_SHIPMENTCOSTNUMBER));
				bean.setShipmentCost(result.getDouble(COL_SHIPMENTCOST));
				bean.setInvoiceSent(Boolean.valueOf(result.getString(COL_INVOICESENT)));
				bean.setAllCostCombined();

				data.add(bean);
			}
			stmt.close();

		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace = sw.toString();
			JOptionPane.showMessageDialog(null, sStackTrace);
		}
		return data;
	}

	public ArrayList<Bean> getAllDataToSend() {
		try {
			stmt = conn.createStatement();
			ArrayList<Bean> data = new ArrayList<>();

			String query = "SELECT * FROM " + TABLE_INVOICE + " WHERE " + COL_INVOICESENT + " = '" + Boolean.FALSE
					+ "' AND trim(" + COL_EMAILADDRESS + ") != '';";

			System.out.println(query);
			ResultSet result = stmt.executeQuery(query);

			while (result.next()) {
				Bean bean = new Bean();
				bean.setCustomerNumber(result.getInt(COL_CUSTOMERNO));
				bean.setInvoiceNumber(result.getInt(COL_INVOICENO));
				bean.setCompanyName(result.getString(COL_COMPANYNAME));
				bean.setPersonName(result.getString(COL_PERSONNAME));
				bean.setCity(result.getString(COL_CITY));
				bean.setZipcode(result.getString(COL_ZIPCODE));
				bean.setAddress(result.getString(COL_ADDRESS));
				bean.setCountry(result.getString(COL_COUNTRY));
				bean.setVatNumber(result.getString(COL_VATNUMBER));
				bean.setEmailAddress(result.getString(COL_EMAILADDRESS));
				bean.setVatPercentage(result.getDouble(COL_VATPERCENTAGE));
				bean.setItemOneDescription(result.getString(COL_ITEM1DESCRIPTION));
				bean.setAmountOfItemsOne(result.getInt(COL_ITEM1AMOUNT));
				bean.setPriceItemOne(result.getDouble(COL_ITEM1PRICE));
				bean.setInvoiceDate(result.getString(COL_INVOICEDATE));
				bean.setItemTwoDescription(result.getString(COL_ITEM2DESCRIPTION));
				bean.setAmountOfItemsTwo(result.getInt(COL_ITEM2AMOUNT));
				bean.setPriceItemTwo(result.getDouble(COL_ITEM2PRICE));
				bean.setItemThreeDescription(result.getString(COL_ITEM3DESCRIPTION));
				bean.setAmountOfItemsThree(result.getInt(COL_ITEM3AMOUNT));
				bean.setPriceItemThree(result.getDouble(COL_ITEM3PRICE));
				bean.setHasShipmentCost(Boolean.valueOf(result.getString(COL_HASSHIPMENTCOST)));
				bean.setShipmentCostNumber(result.getString(COL_SHIPMENTCOSTNUMBER));
				bean.setShipmentCost(result.getDouble(COL_SHIPMENTCOST));
				bean.setInvoiceSent(Boolean.valueOf(result.getString(COL_INVOICESENT)));
				bean.setAllCostCombined();

				data.add(bean);
			}
			stmt.close();
			return data;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void deleteRecord(Bean bean) {
		try {
			stmt = conn.createStatement();
			String query = "DELETE FROM " + TABLE_INVOICE + " WHERE " + COL_INVOICENO + " = " + bean.getInvoiceNumber();

			System.out.println(query);
			stmt.execute(query);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private byte[] readFile(File f) {
		ByteArrayOutputStream bos = null;
		try {

			FileInputStream fis = new FileInputStream(f);
			byte[] buffer = new byte[1024];
			bos = new ByteArrayOutputStream();
			for (int len; (len = fis.read(buffer)) != -1;) {
				bos.write(buffer, 0, len);
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (IOException e2) {
			System.err.println(e2.getMessage());
		}
		return bos != null ? bos.toByteArray() : null;
	}

	public double getCurrentMonthTotal() {
		try {
			stmt = conn.createStatement();

			final String TOTAL_MONTHLY = "TOTAL_MONTHLY";

			String query = "SELECT SUM(" + COL_CUMULATIVECOST + ") AS " + TOTAL_MONTHLY + " FROM  " + TABLE_INVOICE
					+ " WHERE strftime('%Y'," + COL_INVOICEDATE + ") = strftime('%Y',date('now')) AND  "
					+ "strftime('%m'," + COL_INVOICEDATE + ") = strftime('%m',date('now'))";

			// System.out.println(query);
			ResultSet result = stmt.executeQuery(query);
			double total = 0;
			if (result.next()) {
				total = result.getDouble(TOTAL_MONTHLY);
			}
			stmt.close();
			return total;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public double getCurrentYearly() {
		try {
			stmt = conn.createStatement();

			final String TOTAL_MONTHLY = "TOTAL_YEARLY";

			String query = "SELECT SUM(" + COL_CUMULATIVECOST + ") AS " + TOTAL_MONTHLY + " FROM  " + TABLE_INVOICE
					+ " WHERE strftime('%Y'," + COL_INVOICEDATE + ") = strftime('%Y',date('now'))";

			// System.out.println(query);
			ResultSet result = stmt.executeQuery(query);
			double total = 0;
			if (result.next()) {
				total = result.getDouble(TOTAL_MONTHLY);
			}
			stmt.close();
			return total;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public double getShippingCostYearly() {
		try {
			stmt = conn.createStatement();

			final String TOTAL_SHIPPING = "TOTAL_SHPPING";

			String query = "SELECT SUM(" + COL_SHIPMENTCOST + "*" + COL_SHIPMENTCOSTNUMBER + ") AS " + TOTAL_SHIPPING
					+ " FROM  " + TABLE_INVOICE + " WHERE strftime('%Y'," + COL_INVOICEDATE
					+ ") = strftime('%Y',date('now')) AND " + COL_HASSHIPMENTCOST + "='true'";

			// System.out.println(query);
			ResultSet result = stmt.executeQuery(query);
			double total = 0;
			if (result.next()) {
				total = result.getDouble(TOTAL_SHIPPING);
			}
			stmt.close();
			return total;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public double getTotalTurnOver() {
		try {
			stmt = conn.createStatement();

			final String TOTAL_MONTHLY = "TOTAL_YEARLY";

			String query = "SELECT SUM(" + COL_CUMULATIVECOST + ") AS " + TOTAL_MONTHLY + " FROM  " + TABLE_INVOICE
					+ ";";

			// System.out.println(query);
			ResultSet result = stmt.executeQuery(query);
			double total = 0;
			if (result.next()) {
				total = result.getDouble(TOTAL_MONTHLY);
			}
			stmt.close();
			return total;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getLastInvoiceNumber() {

		try {
			stmt = conn.createStatement();

			String query = "SELECT * FROM " + TABLE_INVOICE + " ORDER BY " + COL_INVOICENO + " DESC LIMIT 1";

			// System.out.println(query);
			ResultSet result = stmt.executeQuery(query);
			int invoice = 12344;
			if (result.next()) {
				invoice = result.getInt(COL_INVOICENO);
			}
			stmt.close();
			return invoice;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getLastCustomerNumber(int counterId) {

		try {
			stmt = conn.createStatement();

			String query = "SELECT * FROM " + TABLE_CUSTOMER_NO + " WHERE " + COL_CUSTOMER_NO_ID + " = " + counterId;

			// System.out.println(query);
			ResultSet result = stmt.executeQuery(query);
			int invoice = 10001;
			if (result.next()) {
				invoice = result.getInt(COL_CUSTOMERID_COUNTER);
			}
			stmt.close();
			return invoice;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public double getTurnOverPerInvoiceThisYear() {
		try {
			stmt = conn.createStatement();

			final String TURN_OVER_PER_INVOICE = "TURNOVER_PERINVOICE";

			String query = "SELECT Count(*) AS " + TURN_OVER_PER_INVOICE + " FROM  " + TABLE_INVOICE
					+ " WHERE strftime('%Y'," + COL_INVOICEDATE + ") = strftime('%Y',date('now'))";

			// System.out.println(query);
			ResultSet result = stmt.executeQuery(query);
			int total = 0;
			if (result.next()) {
				total = result.getInt(TURN_OVER_PER_INVOICE);
			}
			stmt.close();
			return getCurrentYearly() / total;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getNumberOfItemsThisYear() {
		try {
			stmt = conn.createStatement();

			final String items1 = "items1";
			final String items2 = "items2";
			final String items3 = "items3";

			String query = "SELECT SUM(" + getNumberOfItemsCaseStatement(COL_ITEM1PRICE, COL_ITEM1AMOUNT) + ") AS "
					+ items1 + ", " + "SUM(" + getNumberOfItemsCaseStatement(COL_ITEM2PRICE, COL_ITEM2AMOUNT) + ") AS "
					+ items2 + ", SUM(" + getNumberOfItemsCaseStatement(COL_ITEM3PRICE, COL_ITEM3AMOUNT) + ") AS "
					+ items3 + " FROM  " + TABLE_INVOICE + " e WHERE strftime('%Y'," + COL_INVOICEDATE
					+ ") = strftime('%Y',date('now'))";

			System.out.println(query);
			ResultSet result = stmt.executeQuery(query);
			int total = 0;
			if (result.next()) {
				total = result.getInt(items1) + result.getInt(items2) + result.getInt(items3);
			}
			stmt.close();
			return total;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	private String getNumberOfItemsCaseStatement(String col1, String col2) {
		String caseStatement = "CASE WHEN e." + col1 + " > 0 THEN e." + col2 + " END";

		return caseStatement;
	}

	public int getNumberOfShipments() {
		try {
			stmt = conn.createStatement();

			final String TOTAL_ITEMS = "TOTAL_SHPPING";

			String query = "SELECT SUM( CAST(" + COL_SHIPMENTCOSTNUMBER + " as INT)) AS " + TOTAL_ITEMS + " FROM  "
					+ TABLE_INVOICE;
			;

			// System.out.println(query);
			ResultSet result = stmt.executeQuery(query);
			int total = 0;
			if (result.next()) {
				total = result.getInt(TOTAL_ITEMS);
			}
			stmt.close();
			return total;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void insertCustomerIdCounter(int counterId, int counter) {
		try {
			stmt = conn.createStatement();
			
			String query = "INSERT INTO " + TABLE_CUSTOMER_NO + "(" + COL_CUSTOMER_NO_ID +", " + COL_CUSTOMERID_COUNTER+")"
					+ "VALUES(" + counterId +"," + counter +");";
			
			stmt.execute(query);
			stmt.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public boolean updateCustomerIdCounter(int counterId, int counter) {
		try {
			stmt = conn.createStatement();
			
			String query = "UPDATE "+ TABLE_CUSTOMER_NO +" SET " + COL_CUSTOMERID_COUNTER +" = " + counter +" "
					+ " WHERE " + COL_CUSTOMER_NO_ID + " = " + counterId;
			
			stmt.execute(query);
			stmt.close();
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		
	}
	public boolean updateCustomerNumber(int invoiceNumber, int counter) {
		try {
			stmt = conn.createStatement();
			
			String query = "UPDATE "+ TABLE_INVOICE +" SET " + COL_CUSTOMERNO +" = " + counter +" "
					+ " WHERE " + COL_INVOICENO + " = " + invoiceNumber;
			
			stmt.execute(query);
			stmt.close();
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		
	}

	/*
	 * public ArrayList<Bean> searchRows(String value) { try { stmt =
	 * conn.createStatement(); ArrayList<Bean> data = new ArrayList<>();
	 * 
	 * String query = "SELECT * FROM " + TABLE_INVOICE_VIRTUAL +" ('"+value+"');";
	 * System.out.println(query); ResultSet result = stmt.executeQuery(query);
	 * 
	 * System.out.println(result.getFetchSize()); while (result.next()) { Bean bean
	 * = new Bean(); bean.setInvoiceNumber(result.getInt(COL_INVOICENO));
	 * bean.setCompanyName(result.getString(COL_COMPANYNAME));
	 * bean.setPersonName(result.getString(COL_PERSONNAME));
	 * bean.setCity(result.getString(COL_CITY));
	 * bean.setZipcode(result.getString(COL_ZIPCODE));
	 * bean.setAddress(result.getString(COL_ADDRESS));
	 * bean.setCountry(result.getString(COL_COUNTRY));
	 * bean.setVatNumber(result.getInt(COL_VATNUMBER));
	 * bean.setEmailAddress(result.getString(COL_EMAILADDRESS));
	 * bean.setVatPercentage(result.getDouble(COL_VATPERCENTAGE));
	 * bean.setItemOneDescription(result.getString(COL_ITEM1DESCRIPTION));
	 * bean.setAmountOfItemsOne(result.getInt(COL_ITEM1AMOUNT));
	 * bean.setPriceItemOne(result.getInt(COL_ITEM1PRICE));
	 * bean.setItemTwoDescription(result.getString(COL_ITEM2DESCRIPTION));
	 * bean.setAmountOfItemsTwo(result.getInt(COL_ITEM2AMOUNT));
	 * bean.setPriceItemTwo(result.getInt(COL_ITEM2PRICE));
	 * bean.setItemThreeDescription(result.getString(COL_ITEM3DESCRIPTION));
	 * bean.setAmountOfItemsThree(result.getInt(COL_ITEM3AMOUNT));
	 * bean.setPriceItemThree(result.getInt(COL_ITEM3PRICE));
	 * bean.setHasShipmentCost(Boolean.valueOf(result.getString(COL_HASSHIPMENTCOST)
	 * )); bean.setShipmentCostNumber(result.getString(COL_SHIPMENTCOSTNUMBER));
	 * bean.setShipmentCost(result.getDouble(COL_SHIPMENTCOST));
	 * bean.setInvoiceSent(Boolean.valueOf(result.getString(COL_INVOICESENT)));
	 * bean.setAllCostCombined();
	 * 
	 * data.add(bean); } System.out.println(query); stmt.close(); return data; }
	 * catch (Exception e) { e.printStackTrace(); } return null; }
	 */

}
