package model;

import java.io.File;

public class Company {

	private File photoPath;
	private String name;
	private String streetNumberName;
	private String zipCodeCity;
	private String country;
	private String kvk;
	private String btw;
	private String bank;
	private String iban;
	private String bacNumber;
	private String tagLine;
	private String tel1;
	private String tel2;
	private String senderEmail;
	private String website;
	private String reinigen;
	
	public Company() {
		
		this.name = "DPF Center";
		this.streetNumberName = "Poolland 24";
		this.zipCodeCity = "1768BX Barsignerhorn";
		this.country = "Nederland";
		this.kvk = "76109135";
		this.btw = "NL860510529B01";
		this.bank = "RABOBANK";
		this.iban = "NL33 RABO 0347 0357 79";
		this.bacNumber = "RABONL2U";
		this.tagLine = "Onderdeel van Verleij Services BV";
		this.tel1 = "";
		this.tel2 = "";
		this.senderEmail = "invoice@verleijtradingeurope.nl";
		this.reinigen = "Reinigen roetflter";
		this.website = "";
	}

	public String getWebsite() {
		return website;
	}
	
	public void setWebsite(String website) {
		this.website = website;
	}
	
	
	public String getSenderEmail() {
		return senderEmail;
	}
	
	public String getReinigen() {
		return reinigen;
	}

	public void setReinigen(String reinigen) {
		this.reinigen = reinigen;
	}


	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}



	public String getTel1() {
		return tel1;
	}



	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}



	public String getTel2() {
		return tel2;
	}



	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}



	public String getTagLine() {
		return tagLine;
	}


	public void setTagLine(String tagLine) {
		this.tagLine = tagLine;
	}


	public File getPhotoPath() {
		return photoPath;
	}


	public void setPhotoPath(File photoPath) {
		this.photoPath = photoPath;
	}


	public void setName(String name) {
		this.name = name;
	}

	public void setCountry1(String country1) {
		this.streetNumberName = country1;
	}

	public void setAddress1(String address1) {
		this.zipCodeCity = address1;
	}

	public void setCountry2(String country2) {
		this.country = country2;
	}

	public void setKvk(String kvk) {
		this.kvk = kvk;
	}

	public void setBtw(String btw) {
		this.btw = btw;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public void setBic(String bic) {
		this.bacNumber = bic;
	}

	public String getName() {
		return name;
	}

	public String getCountry1() {
		return streetNumberName;
	}

	public String getAddress1() {
		return zipCodeCity;
	}

	public String getCountry2() {
		return country;
	}

	public String getKvk() {
		return kvk;
	}

	public String getBtw() {
		return btw;
	}

	public String getBank() {
		return bank;
	}

	public String getIban() {
		return iban;
	}

	public String getBic() {
		return bacNumber;
	}
	
	
}
