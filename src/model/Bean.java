package model;

public class Bean {

	private int customerNumber;
	private int invoiceNumber;
	private String companyName;
	private String personName;
	private String address;
	private String zipcode;
	private String city;
	private String country;
	private String vatNumber;
	private String emailAddress;
	private double vatPercentage;
	private String itemOneDescription;
	private int amountOfItemsOne;
	private double priceItemOne;
	private String invoiceDate;
	private String itemTwoDescription;
	private int amountOfItemsTwo;
	private double priceItemTwo;
	private String itemThreeDescription;
	private int amountOfItemsThree;
	private double priceItemThree;
	private boolean hasShipmentCost;
	private String shipmentCostNumber;
	private double shipmentCost;
	private boolean invoiceSent;
	private double allCostCombined;

	public Bean() {
		this.customerNumber = 10001;
		invoiceNumber = 12345;
		companyName = "";
		personName = "";
		address = "";
		zipcode = "";
		city = "";
		country = "";
		vatNumber = "";
		emailAddress = "";
		vatPercentage = 21;
		itemOneDescription = "";
		amountOfItemsOne = 0;
		priceItemOne = 0;
		invoiceDate = Utils.getTodayDate();
		itemTwoDescription = "";
		amountOfItemsTwo = 0;
		priceItemTwo = 0;
		itemThreeDescription = "";
		amountOfItemsThree = 0;
		priceItemThree = 0;
		hasShipmentCost = false;
		shipmentCostNumber = "1";
		shipmentCost = 0;
		invoiceSent = false;
		allCostCombined = 0;
	}

	public Bean(Bean bean) {
		// invoiceNumber = bean.getInvoiceNumber() + 1;
		
		this.customerNumber = bean.customerNumber + 1;
		companyName = bean.getCompanyName();
		setPersonName(bean.getPersonName());
		setAddress(bean.getAddress());
		setZipcode(bean.getZipcode());
		setCity(bean.getCity());
		setCountry(bean.getCountry());
		setVatNumber(bean.getVatNumber());
		setEmailAddress(bean.getEmailAddress());
		setVatPercentage(bean.getVatPercentage());
		setItemOneDescription(bean.getItemOneDescription());
		setAmountOfItemsOne(bean.getAmountOfItemsOne());
		setPriceItemOne(bean.getPriceItemOne());
		invoiceDate = Utils.getTodayDate();
		setItemTwoDescription(bean.getItemTwoDescription());
		setAmountOfItemsTwo(bean.getAmountOfItemsTwo());
		setPriceItemTwo(bean.getPriceItemTwo());
		setItemThreeDescription(bean.getItemThreeDescription());
		setAmountOfItemsThree(bean.getAmountOfItemsThree());
		setPriceItemThree(bean.getPriceItemThree());
		setHasShipmentCost(bean.hasShipmentCost);
		setShipmentCostNumber(bean.getShipmentCostNumber());
		setShipmentCost(bean.getShipmentCost());
		setInvoiceSent(false);
		setAllCostCombined();
	}

	@Override
	public String toString() {
		return invoiceNumber + "," + companyName + "," + personName + "," + address + "," + zipcode + "," + city + ","
				+ country + "," + vatNumber + "," + emailAddress + "," + vatPercentage + "," + itemOneDescription + ","
				+ amountOfItemsOne + "," + priceItemOne + "," + invoiceDate + "," + itemTwoDescription + ","
				+ priceItemTwo + "," + amountOfItemsTwo + "," + itemThreeDescription + "," + amountOfItemsThree + ","
				+ priceItemThree + "," + hasShipmentCost + "," + shipmentCostNumber + "," + shipmentCost + ","
				+ invoiceSent + "," + allCostCombined;
	}
	
	public int getCustomerNumber() {
		return customerNumber;
	}
	
	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public int getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(int invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		if (vatNumber.trim().isEmpty()) {
			setVatPercentage(21);
		} else {
			setVatPercentage(0);
		}
		this.vatNumber = vatNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public double getVatPercentage() {
		return vatPercentage;
	}

	public void setVatPercentage(double vatPercentage) {
		this.vatPercentage = vatPercentage;
	}

	public String getItemOneDescription() {
		return itemOneDescription;
	}

	public void setItemOneDescription(String itemOneDescription) {
		this.itemOneDescription = itemOneDescription;
	}

	public int getAmountOfItemsOne() {
		return amountOfItemsOne;
	}

	public void setAmountOfItemsOne(int amountOfItemsOne) {
		this.amountOfItemsOne = amountOfItemsOne;
		setAllCostCombined();
	}

	public double getPriceItemOne() {
		return priceItemOne;
	}

	public void setPriceItemOne(double priceItemOne) {
		this.priceItemOne = priceItemOne;
		setAllCostCombined();
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getItemTwoDescription() {
		return itemTwoDescription;
	}

	public void setItemTwoDescription(String itemTwoDescription) {
		this.itemTwoDescription = itemTwoDescription;
	}

	public int getAmountOfItemsTwo() {
		return amountOfItemsTwo;
	}

	public void setAmountOfItemsTwo(int amountOfItemsTwo) {
		this.amountOfItemsTwo = amountOfItemsTwo;
		setAllCostCombined();
	}

	public double getPriceItemTwo() {
		return priceItemTwo;
	}

	public void setPriceItemTwo(double priceItemTwo) {
		this.priceItemTwo = priceItemTwo;
		setAllCostCombined();
	}

	public String getItemThreeDescription() {
		return itemThreeDescription;
	}

	public void setItemThreeDescription(String itemThreeDescription) {
		this.itemThreeDescription = itemThreeDescription;
	}

	public int getAmountOfItemsThree() {
		return amountOfItemsThree;
	}

	public void setAmountOfItemsThree(int amountOfItemsThree) {
		this.amountOfItemsThree = amountOfItemsThree;
		setAllCostCombined();
	}

	public double getPriceItemThree() {
		return priceItemThree;
	}

	public void setPriceItemThree(double priceItemThree) {
		this.priceItemThree = priceItemThree;
		setAllCostCombined();
	}

	public boolean isHasShipmentCost() {
		return hasShipmentCost;
	}

	public void setHasShipmentCost(boolean hasShipmentCost) {
		this.hasShipmentCost = hasShipmentCost;
	}

	public String getShipmentCostNumber() {
		return shipmentCostNumber;
	}

	public void setShipmentCostNumber(String shipmentCostNumber) {
		this.shipmentCostNumber = shipmentCostNumber;
		setAllCostCombined();
	}

	public double getShipmentCost() {
		return shipmentCost;
	}

	public void setShipmentCost(double shipmentCost) {
		this.shipmentCost = shipmentCost;
		setAllCostCombined();
	}

	public boolean isInvoiceSent() {
		return invoiceSent;
	}

	public void setInvoiceSent(boolean invoiceSent) {
		this.invoiceSent = invoiceSent;
	}

	public double getAllCostCombined() {
		return allCostCombined;
	}

	public void setAllCostCombined() {
		this.allCostCombined = setPrice();
	}

	private double setPrice() {
		double total = 0;
		total += amountOfItemsOne * priceItemOne;
		total += amountOfItemsTwo * priceItemTwo;
		total += amountOfItemsThree * priceItemThree;
		total += hasShipmentCost ? (shipmentCost * Double.parseDouble(this.getShipmentCostNumber())) : 0;
		return total;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bean) {
			Bean bean = (Bean) obj;
			return bean.getInvoiceNumber() == this.invoiceNumber;
		}
		return false;
	}

}
