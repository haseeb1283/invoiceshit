

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;


import listeners.InvoiceStatusChangeListener;
import listeners.RefreshStatsChangeListener;
import listeners.UpdateBeanListener;
import model.Bean;
import model.Utils;

public class TableModel extends AbstractTableModel {

	public static final String COLUMN_NAMES[] = { "CUSTOMER NO.", "INVOICE N0.", "COMPANY NAME", "PERSON NAME", "ADDRESS", "ZIP CODE",
			"CITY", "COUNTRY", "VAT NUMBER", "EMAIL ADDRESS", "VAT %", "ITEM 1 DESCRIPTION", "AMOUNT OF ITEM(S) 1",
			"PRICE ITEM 1 (EX VAT)", "INVOICE DATE", "ITEM 2 DESCRIPION", "AMOUNT OF ITEMS(S) 2",
			"PRICE ITEM 2 (EX VAT)", "ITEM 3 DESCRIPION", "AMOUNT OF ITEMS(S) 3", "PRICE ITEM 3 (EX VAT)",
			"SHIPMENT COST", "SHIPMENT COST NUMBER", "SHIPMENT COST", "INVOICE SENT STATUS", "CUMULATIVE COST" };

	private ArrayList<Bean> data;
	private UpdateBeanListener updateBeanListener;
	private InvoiceStatusChangeListener invoiceStatusChangeListener;
	private RefreshStatsChangeListener refreshStatsChangeListener;

	public TableModel(ArrayList<Bean> data) {
		this.data = data;
	}

	public void setUpdateBeanListener(UpdateBeanListener updateBeanListener) {
		this.updateBeanListener = updateBeanListener;
	}

	public void setRefreshStatsChangeListener(RefreshStatsChangeListener refreshStatsChangeListener) {
		this.refreshStatsChangeListener = refreshStatsChangeListener;
	}

	@Override
	public String getColumnName(int column) {
		return COLUMN_NAMES[column];
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public int getColumnCount() {
		return COLUMN_NAMES.length;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 1:
			return !data.get(rowIndex).isInvoiceSent();
		case 9:
		case 24:
		case 25:
			return false;
		}
		return true;
	}

	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		if (value == null) {
			return;
		}
		Bean bean = (Bean) data.get(rowIndex);

		int oldInvoiceNumber = bean.getInvoiceNumber();
		switch (columnIndex) {
		case 0:
			bean.setCustomerNumber((Integer)value);
		case 1:
			bean.setInvoiceNumber((Integer) value);
			break;
		case 2:
			bean.setCompanyName((String) value);
			break;
		case 3:
			bean.setPersonName((String) value);
			break;
		case 4:
			bean.setAddress((String) value);
			break;
		case 5:
			bean.setZipcode((String) value);
			break;
		case 6:
			bean.setCity((String) value);
			break;
		case 7:
			bean.setCountry((String) value);
			break;
		case 8:
			bean.setVatNumber((String) value);
			break;
		case 9:
			bean.setEmailAddress((String) value);
			break;
		case 10:
			bean.setVatPercentage((Double) value);
			break;
		case 11:
			bean.setItemOneDescription((String) value);
			break;
		case 12:
			try {
				String aValue = (String) value;
				if (!aValue.trim().isEmpty() && Integer.parseInt(aValue) >= 0)
					bean.setAmountOfItemsOne(Integer.parseInt((String) value));
				else
					bean.setAmountOfItemsOne(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 13:
			try {
				String pValue = (String) value;
				if (!pValue.trim().isEmpty()) {
					bean.setPriceItemOne(Double.parseDouble(pValue));
				} else {
					bean.setPriceItemOne(0);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 14:
			try {
				String date = (String) value;
				bean.setInvoiceDate(Utils.getDatabaseDate(date));
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 15:
			bean.setItemTwoDescription((String) value);
			break;
		case 16:
			try {
				String aValue2 = (String) value;
				if (!aValue2.trim().isEmpty() && Integer.parseInt(aValue2) >= 0)
					bean.setAmountOfItemsTwo(Integer.parseInt((String) value));
				else
					bean.setAmountOfItemsTwo(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 17:
			try {
				String pValue2 = (String) value;
				if (!pValue2.trim().isEmpty()) {
					bean.setPriceItemTwo(Double.parseDouble(pValue2));
				} else {
					bean.setPriceItemTwo(0);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 18:
			bean.setItemThreeDescription((String) value);
			break;
		case 19:
			try {
				String aValue3 = (String) value;
				if (!aValue3.trim().isEmpty() && Integer.parseInt(aValue3) >= 0)
					bean.setAmountOfItemsThree(Integer.parseInt((String) value));
				else
					bean.setAmountOfItemsThree(0);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 20:
			try {
				String pValue3 = (String) value;
				if (!pValue3.trim().isEmpty()) {
					bean.setPriceItemThree(Double.parseDouble(pValue3));
				} else {
					bean.setPriceItemThree(0);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 21:
			bean.setHasShipmentCost((Boolean) value);
			bean.setAllCostCombined();
			break;
		case 22:
			bean.setShipmentCostNumber((String) value);
			break;
		case 23:
			try {
				String sValue = (String) value;
				if (!sValue.trim().isEmpty()) {
					bean.setShipmentCost(Double.parseDouble(sValue));
				} else {
					bean.setShipmentCost(0);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case 24:
			bean.setInvoiceSent((Boolean) value);
			if (invoiceStatusChangeListener != null) {
				invoiceStatusChangeListener.sentStatusChanged();
			}
			break;
		case 25:
			bean.setAllCostCombined();
		}

		if (updateBeanListener != null){
			updateBeanListener.updateBean(oldInvoiceNumber, bean);
		}
		if (refreshStatsChangeListener != null) {
			refreshStatsChangeListener.refreshStats();
		}

		fireTableRowsUpdated(rowIndex, columnIndex);
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return Integer.class;
		case 1:
			return Integer.class;
		case 2:
			return String.class;
		case 3:
			return String.class;
		case 4:
			return String.class;
		case 5:
			return String.class;
		case 6:
			return String.class;
		case 7:
			return String.class;
		case 8:
			return String.class;
		case 9:
			return String.class;
		case 10:
			return Double.class;
		case 11:
			return String.class;
		case 12:
			return String.class;
		case 13:
			return String.class;
		case 14:
			return String.class;
		case 15:
			return String.class;
		case 16:
			return String.class;
		case 17:
			return String.class;
		case 18:
			return String.class;
		case 19:
			return String.class;
		case 20:
			return String.class;
		case 21:
			return Boolean.class;
		case 22:
			return String.class;
		case 23:
			return String.class;
		case 24:
			return Boolean.class;
		case 25:
			return Double.class;
		}
		return null;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Bean bean = (Bean) data.get(rowIndex);

		switch (columnIndex) {
		case 0:
			return bean.getCustomerNumber();
		case 1:
			return bean.getInvoiceNumber();
		case 2:
			return bean.getCompanyName();
		case 3:
			return bean.getPersonName();
		case 4:
			return bean.getAddress();
		case 5:
			return bean.getZipcode();
		case 6:
			return bean.getCity();
		case 7:
			return bean.getCountry();
		case 8:
			return bean.getVatNumber();
		case 9:
			return bean.getEmailAddress();
		case 10:
			return bean.getVatPercentage();
		case 11:
			return bean.getItemOneDescription();
		case 12:
			return bean.getAmountOfItemsOne() == 0 ? "" : bean.getAmountOfItemsOne();
		case 13:
			return bean.getPriceItemOne() == 0 ? "" : bean.getPriceItemOne();
		case 14:
			return Utils.getDisplayDate(bean.getInvoiceDate());
		case 15:
			return bean.getItemTwoDescription();
		case 16:
			return bean.getAmountOfItemsTwo() == 0 ? "" : bean.getAmountOfItemsTwo();
		case 17:
			return bean.getPriceItemTwo() == 0 ? "" : bean.getPriceItemTwo();
		case 18:
			return bean.getItemThreeDescription();
		case 19:
			return bean.getAmountOfItemsThree() == 0 ? "" : bean.getAmountOfItemsThree();
		case 20:
			return bean.getPriceItemThree() == 0 ? "" : bean.getPriceItemThree();
		case 21:
			return bean.isHasShipmentCost();
		case 22:
			return bean.getShipmentCostNumber();
		case 23:
			return bean.getShipmentCost() == 0 ? "" : bean.getShipmentCost();
		case 24:
			return bean.isInvoiceSent();
		case 25:
			return bean.getAllCostCombined();
		}
		return null;
	}

	public void setInvoiceStatusChangedListener(InvoiceStatusChangeListener invoiceStatusChangeListener) {
		this.invoiceStatusChangeListener = invoiceStatusChangeListener;
	}

}
