import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import listeners.UpdateCompanyListener;
import model.Bean;
import model.Company;
import model.Database;
import model.Utils;

public class DisplayDetailsPanel extends JPanel {

	JPanel logoPathPanel;
	JLabel logoPath;
	JButton logoPathBtn;

	JPanel otherDetailsPanel;
	JTextField companyName;
	JTextField streetNumber;
	JTextField zipCodeCity;
	JTextField country;
	JTextField kvk;
	JTextField btw;
	JTextField bank;
	JTextField iban;
	JTextField bicNumber;
	Company company;
	JButton saveDetails;
	JTextField tagLine;
	JTextField tel1;
	JTextField tel2;
	JTextField senderEmail;
	JTextField website;
	JTextField reinigen;

	private UpdateCompanyListener updateCompanyListener;
	JFileChooser chooser;

	public DisplayDetailsPanel() {
		super(new BorderLayout());

		logoPathPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		logoPathPanel.setBorder(BorderFactory.createTitledBorder("Change logo path"));
		logoPathBtn = new JButton("Change Logo");
		logoPath = new JLabel();
		logoPath.setPreferredSize(new Dimension(400, 200));

		logoPathPanel.add(logoPath);
		logoPathPanel.add(logoPathBtn);

		chooser = new JFileChooser(".");
		chooser.setFileFilter(new FileNameExtensionFilter("Image Files (*.png) (*.jpg)", "png", "jpg", "jpeg"));
		logoPathBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (company != null) {
					int choice = chooser.showOpenDialog(DisplayDetailsPanel.this);
					if (choice == JFileChooser.APPROVE_OPTION) {
						company.setPhotoPath(chooser.getSelectedFile());
						setLogo(chooser.getSelectedFile());
						Database.getInstance().updateCompanyDetails(company, Utils.COMPANY_ID);

					}
				}
			}
		});

		otherDetailsPanel = new JPanel(new BorderLayout());
		otherDetailsPanel.setBorder(BorderFactory.createTitledBorder("Other Details"));
		JPanel temp = new JPanel(new GridLayout(16, 1, 5, 10));
		temp.setBorder(BorderFactory.createEmptyBorder(50, 250, 50, 250));

		temp.add(new JLabel("Company Name: "));
		companyName = new JTextField();
		temp.add(companyName);

		temp.add(new JLabel("Streetname+number: "));
		streetNumber = new JTextField();
		temp.add(streetNumber);

		temp.add(new JLabel("Zipcode+city: "));
		zipCodeCity = new JTextField();
		temp.add(zipCodeCity);

		temp.add(new JLabel("Country: "));
		country = new JTextField();
		temp.add(country);

		temp.add(new JLabel("KvK nr: "));
		kvk = new JTextField();
		temp.add(kvk);

		temp.add(new JLabel("BTW nr: "));
		btw = new JTextField();
		temp.add(btw);

		temp.add(new JLabel("Bank: "));
		bank = new JTextField();
		temp.add(bank);

		temp.add(new JLabel("IBAN: "));
		iban = new JTextField();
		temp.add(iban);

		temp.add(new JLabel("BIC number: "));
		bicNumber = new JTextField();
		temp.add(bicNumber);

		temp.add(new JLabel("Tagline: "));
		tagLine = new JTextField();
		temp.add(tagLine);

		temp.add(new JLabel("Sender Email: "));
		senderEmail = new JTextField();
		temp.add(senderEmail);

		temp.add(new JLabel("Tel1: "));
		tel1 = new JTextField();
		temp.add(tel1);

		temp.add(new JLabel("Tel2: "));
		tel2 = new JTextField();
		temp.add(tel2);
		

		temp.add(new JLabel("Website: "));
		website = new JTextField();
		temp.add(website);
		
		temp.add(new JLabel("Reinigen roetflter: "));
		reinigen = new JTextField();
		temp.add(reinigen);

		temp.add(new JLabel(""));

		JPanel temp1 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		saveDetails = new JButton("Save");
		temp1.add(saveDetails);

		saveDetails.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (updateCompanyListener != null && company != null) {
					setNewValues();
					updateCompanyListener.onCompanyUpdated(company);
				}
			}
		});

		temp.add(temp1);

		otherDetailsPanel.add(temp, BorderLayout.NORTH);
		add(logoPathPanel, BorderLayout.NORTH);
		add(new JScrollPane(otherDetailsPanel), BorderLayout.CENTER);

		setValues();
	}

	private void setNewValues() {
		if (this.company != null) {
			this.company.setName(this.companyName.getText());
			this.company.setAddress1(this.zipCodeCity.getText());
			this.company.setCountry1(this.streetNumber.getText());
			this.company.setCountry2(this.country.getText());
			this.company.setKvk(this.kvk.getText());
			this.company.setBtw(this.btw.getText());
			this.company.setBank(this.bank.getText());
			this.company.setIban(this.iban.getText());
			this.company.setBic(this.bicNumber.getText());
			this.company.setTagLine(this.tagLine.getText());
			this.company.setSenderEmail(this.senderEmail.getText());
			this.company.setTel1(this.tel1.getText());
			this.company.setTel2(this.tel2.getText());
			this.company.setWebsite(this.website.getText());
			this.company.setReinigen(this.reinigen.getText());
		}
	}

	public void setValues() {
		this.company = Database.getInstance().getCompanyDetails(Utils.COMPANY_ID);
		if (this.company != null) {
			if (company.getPhotoPath() != null) {
				setLogo(company.getPhotoPath());
			} else {
				logoPath.setIcon(null);
			}
			this.companyName.setText(company.getName());
			this.streetNumber.setText(company.getCountry1());
			this.zipCodeCity.setText(company.getAddress1());
			this.country.setText(company.getCountry2());
			this.kvk.setText(company.getKvk());
			this.btw.setText(company.getBtw());
			this.bank.setText(company.getBank());
			this.iban.setText(company.getIban());
			this.bicNumber.setText(company.getBic());
			this.tagLine.setText(company.getTagLine());
			this.senderEmail.setText(company.getSenderEmail());
			this.tel1.setText(company.getTel1());
			this.tel2.setText(company.getTel2());
			this.website.setText(company.getWebsite());
			this.reinigen.setText(company.getReinigen());
		}
	}

	private void setLogo(File file) {
		
		/*
		 * Image dimg = img.getScaledInstance(logoPath.getWidth(), logoPath.getHeight(),
		 * Image.SCALE_SMOOTH);
		 */
		ImageIcon imgThisImg = new ImageIcon(file.getAbsolutePath());
		logoPath.setIcon(imgThisImg);

	}

	public void setDefaultValues() {
		this.company = null;
		this.companyName.setText("");
		this.streetNumber.setText("");
		this.zipCodeCity.setText("");
		this.country.setText("");
		this.kvk.setText("");
		this.btw.setText("");
		this.bank.setText("");
		this.iban.setText("");
		this.bicNumber.setText("");
		this.tagLine.setText("");
		this.website.setText("");
	}

	public void setUpdateCompanyListener(UpdateCompanyListener updateCompanyListener) {
		this.updateCompanyListener = updateCompanyListener;
	}
}
