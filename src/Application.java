import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import com.itextpdf.text.Document;

import listeners.AddNewRowListener;
import listeners.CopyRowListener;
import listeners.DeleteRowListener;
import listeners.PasteRowListener;
import listeners.RefreshStatsChangeListener;
import listeners.SearchRowListener;
import listeners.SendInvoiceListener;
import listeners.TableRowSelectListener;
import listeners.UpdateBeanListener;
import listeners.UpdateCompanyListener;
import listeners.customerIdUpdateListener;
import model.Bean;
import model.Company;
import model.Database;
import model.Utils;

public class Application extends JFrame {

	TablePanel tablePanel;
	Toolbar toolbar;
	Database database;

	JTabbedPane tabbedPane;
	DisplayDetailsPanel displayDetailsPanel;
	ServerDetailsPanel serverDetailsPanel;

	public Application() {
		setLayout(new BorderLayout());

		tabbedPane = new JTabbedPane();
		database = Database.getInstance();
		toolbar = new Toolbar();
		
		
		toolbar.setAddNewRowListener(new AddNewRowListener() {

			@Override
			public void addNewBean() {
				tablePanel.addNewRow();
			}
		});
		toolbar.setDeleteRowListener(new DeleteRowListener() {

			@Override
			public void deleteRow() {
				tablePanel.deleteRow();
			}
		});

		toolbar.setCopyRowListener(new CopyRowListener() {

			@Override
			public void copyRow(boolean copyToApp) {
				// TODO Auto-generated method stub
				tablePanel.copyRow(copyToApp);
			}
		});

		toolbar.setSearchRowListener(new SearchRowListener() {

			@Override
			public void searchRow(String text) {
				tablePanel.searchTable(text);
			}
		});
		
		toolbar.setPasteRowListener(new PasteRowListener() {
			
			@Override
			public void pasteRow() {
				tablePanel.pasteRow();
			}
		});

		toolbar.setSendInvoiceListener(new SendInvoiceListener() {

			@Override
			public void sendInvoice() {
				if (Database.getInstance().getCompanyDetails(Utils.COMPANY_ID).getPhotoPath() == null) {
					JOptionPane.showMessageDialog(Application.this, "Upload company logo before sending invoice",
							"Company logo not set", JOptionPane.ERROR_MESSAGE);
				} else {
					Application.this.sendInvoice();
				}
			}
		});

		tablePanel = new TablePanel(database.getAllData());

		tablePanel.add(toolbar, BorderLayout.NORTH);
		
		tablePanel.setCustomerIdUpdateListener(new customerIdUpdateListener() {
			
			@Override
			public void onCustomerIdUpdated() {
				toolbar.updateCustomerIdField();
				
			}
		});

		displayDetailsPanel = new DisplayDetailsPanel();
		displayDetailsPanel.setUpdateCompanyListener(new UpdateCompanyListener() {

			@Override
			public void onCompanyUpdated(Company company) {
				updateCompanyDetails(company);
			}
		});

		tablePanel.setUpdateBeanListener(new UpdateBeanListener() {

			@Override
			public void updateBean(int oldInvoiceNumber, Bean bean) {
				// TODO Auto-generated method stub
				database.updateRow(oldInvoiceNumber, bean);
			}
		});

		tablePanel.setTableRowSelectedListener(new TableRowSelectListener() {

			@Override
			public void isRowSelected(boolean isSelected, Bean bean) {
				toolbar.setSendInvoiceButtonEnabled(isSelected, bean);
				/*
				 * if (bean != null) { displayDetailsPanel.setValues(bean); } else {
				 * displayDetailsPanel.setDefaultValues(); }
				 */
			}
		});
		
		tablePanel.setRefreshStatsChangeListener(new RefreshStatsChangeListener() {
			
			@Override
			public void refreshStats() {
				toolbar.refreshStats();
			}
		});

		tabbedPane.addTab("Invoice Table", tablePanel);
		tabbedPane.addTab("Company Details", new JScrollPane(displayDetailsPanel));
		
		serverDetailsPanel = new ServerDetailsPanel();
		tabbedPane.addTab("Server Details", new JScrollPane(serverDetailsPanel));

		add(tabbedPane, BorderLayout.CENTER);

		pack();
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("logo.png"));

		setVisible(true);
		// setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);

	}

	private void sendInvoice() {
		PdfGenerator generator = PdfGenerator.getInstance();
		new Thread(new Runnable() {

			@Override
			public void run() {
				toolbar.setProgressBarVisibility(true);
				// Bean bean = tablePanel.getSelectedBean();
				ArrayList<Bean> toSend = Database.getInstance().getAllDataToSend();
				toolbar.setMaximumProgress(toSend.size());

				int sent = 1;
				for (Bean bean : toSend) {
					//bean.setInvoiceDate(Utils.getTodayDate());
					generator.generatePdf(bean, Database.getInstance().getCompanyDetails(Utils.COMPANY_ID));
					SendEmail.sendMail(bean, Database.getInstance().getCompanyDetails(Utils.COMPANY_ID));
					tablePanel.setUpdateInvoiceSentColumn(bean);
					toolbar.setProgress(sent++);
					toolbar.refreshStats();
				}
				toolbar.setProgressBarVisibility(false);
			}
		}).start();

	}

	private void updateCompanyDetails(Company company) {
		Database.getInstance().updateCompanyDetails(company, Utils.COMPANY_ID);
	}
}
