package listeners;

import model.Bean;

public interface TableRowSelectListener {

	void isRowSelected(boolean isSelected, Bean bean);
}
