package listeners;

public interface CopyRowListener {

	public void copyRow(boolean copyToApp);
}
