package listeners;

import model.Bean;

public interface UpdateBeanListener {

	void updateBean(int oldInvoiceNumber, Bean bean);
}
