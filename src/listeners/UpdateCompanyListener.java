package listeners;

import model.Company;

public interface UpdateCompanyListener {

	void onCompanyUpdated(Company company);
}
