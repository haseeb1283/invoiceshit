package listeners;

public interface RefreshStatsChangeListener {

	void refreshStats();
}
