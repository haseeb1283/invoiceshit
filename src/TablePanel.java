import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;

import listeners.DeleteRowListener;
import listeners.InvoiceStatusChangeListener;
import listeners.RefreshStatsChangeListener;
import listeners.TableRowSelectListener;
import listeners.UpdateBeanListener;
import listeners.customerIdUpdateListener;
import model.Bean;
import model.Database;
import model.Utils;

public class TablePanel extends JPanel {

	JTable table;
	TableModel tableModel;
	ArrayList<Bean> data;
	ArrayList<Bean> temp;
	private TableRowSelectListener tableRowSelectListener;

	private final int COLUMN_WIDTHS[] = { 60, 60, 140, 160, 100, 120, 120, 100, 160, 100, 350, 80, 100, 120, 350, 80,
			100, 350, 80, 100, 100, 100, 100, 100, 100 };

	private UpdateBeanListener updateBeanListener;

	public TablePanel(ArrayList<Bean> data) {
		this.data = data;
		this.temp = data;
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder("Data"));

		table = new JTable();
		tableModel = new TableModel(data);
		tableModel.addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent e) {
				// TODO Auto-generated method stub
				System.out.println("d");
			}
		});

		table.setModel(tableModel);
		InputMap im = table.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
		KeyStroke f2 = KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0);
		im.put(enter, im.get(f2));

		// table.setSelectionBackground(Color.LIGHT_GRAY);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setDefaultRenderer(Object.class, new CellHighlighterRenderer());
		table.setDefaultRenderer(Integer.class, new CellHighlighterRenderer());
		table.setDefaultRenderer(Double.class, new CellHighlighterRenderer());
		// table.setCellSelectionEnabled(true);

		JScrollPane pane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		add(pane, BorderLayout.CENTER);

		for (int i = 0; i < COLUMN_WIDTHS.length; i++) {
			table.getColumnModel().getColumn(i).setPreferredWidth(COLUMN_WIDTHS[i] + 50);
		}
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				if (table.getSelectedRow() != -1 && (table.getSelectionModel().getMinSelectionIndex() == table
						.getSelectionModel().getMaxSelectionIndex())) {
					if (tableRowSelectListener != null) {

						tableRowSelectListener.isRowSelected(true, data.get(table.getSelectedRow()));
					}
				} else {
					if (tableRowSelectListener != null) {
						tableRowSelectListener.isRowSelected(false, null);
					}
				}
			}
		});

		tableModel.setInvoiceStatusChangedListener(new InvoiceStatusChangeListener() {

			@Override
			public void sentStatusChanged() {
				if (table.getSelectedRow() != -1 && (table.getSelectionModel().getMinSelectionIndex() == table
						.getSelectionModel().getMaxSelectionIndex())) {
					if (tableRowSelectListener != null) {

						tableRowSelectListener.isRowSelected(true, data.get(table.getSelectedRow()));
					}
				} else {
					if (tableRowSelectListener != null) {
						tableRowSelectListener.isRowSelected(false, null);
					}
				}
			}
		});
		table.setRowHeight(20);
		table.setAutoCreateColumnsFromModel(false);
		JTable freezeTable = new javax.swing.JTable();
		freezeTable.setAutoCreateColumnsFromModel(false);
		freezeTable.setModel(tableModel);
		freezeTable.setSelectionModel(table.getSelectionModel());
		freezeTable.setFocusable(false);
		freezeTable.setRowHeight(20);
		table.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				// Keep freezeTable in sync with the jTable
				if ("selectionModel".equals(e.getPropertyName())) {
					freezeTable.setSelectionModel(table.getSelectionModel());
				}

				if ("dataModel".equals(e.getPropertyName())) {
					freezeTable.setModel(table.getModel());
				}
			}
		});

		for (int i = 0; i < 1; i++) {
			TableColumnModel colModel = table.getColumnModel();
			TableColumn column = colModel.getColumn(0);
			TableColumn column2 = colModel.getColumn(1);
			colModel.removeColumn(column);
			colModel.removeColumn(column2);
			freezeTable.getColumnModel().addColumn(column);
			freezeTable.getColumnModel().addColumn(column2);
		}

		freezeTable.setPreferredScrollableViewportSize(freezeTable.getPreferredSize());
		pane.setRowHeaderView(freezeTable);
		pane.getRowHeader().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				// Sync the scroll pane scrollbar with the row header
				JViewport viewport = (JViewport) e.getSource();
				pane.getVerticalScrollBar().setValue(viewport.getViewPosition().y);
			}
		});
		pane.setCorner(JScrollPane.UPPER_LEFT_CORNER, freezeTable.getTableHeader());

		setPreferredSize(new Dimension(960, 560));
		// table.scrollRectToVisible(table.getCellRect(table.getRowCount() - 1, 0,
		// true));

		scrollToVisible(table, data.size() - 1, 0);
	
	}

	public static void scrollToVisible(JTable table, int rowIndex, int vColIndex) {
		if (!(table.getParent() instanceof JViewport)) {
			System.out.println("null");
			return;
		}
		JViewport viewport = (JViewport) table.getParent();
		// This rectangle is relative to the table where the
		// northwest corner of cell (0,0) is always (0,0).
		Rectangle rect = table.getCellRect(rowIndex, vColIndex, true);
		// The location of the viewport relative to the table
		Point pt = viewport.getViewPosition();
		// Translate the cell location so that it is relative
		// to the view, assuming the northwest corner of the
		// view is (0,0)
		rect.setLocation(rect.x - pt.x, rect.y - pt.y);
		table.scrollRectToVisible(rect);
		// Scroll the area into view
		// viewport.scrollRectToVisible(rect);
	}

	public class CellHighlighterRenderer extends DefaultTableCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public Component getTableCellRendererComponent(JTable table, Object obj, boolean isSelected, boolean hasFocus,
				int row, int column) {

			Component cell = super.getTableCellRendererComponent(table, obj, isSelected, hasFocus, row, column);

			if (isSelected) {
				if (column == table.getSelectedColumn() && row == table.getSelectedRow() && hasFocus)
					cell.setBackground(Color.orange);
			} else {

				cell.setBackground(new Color(0xFFFFFF));
			}
			return this;
		}
	}

	public void setUpdateBeanListener(UpdateBeanListener updateBeanListener) {
		this.updateBeanListener = updateBeanListener;
		tableModel.setUpdateBeanListener(updateBeanListener);
	}

	public void setTableRowSelectedListener(TableRowSelectListener tableRowSelectListener) {
		this.tableRowSelectListener = tableRowSelectListener;
	}
	
	private customerIdUpdateListener customerIdUpdateListener;
	
	public void setCustomerIdUpdateListener(customerIdUpdateListener customerIdUpdateListener) {
		this.customerIdUpdateListener = customerIdUpdateListener;
	}

	public void addNewRow() {

		Bean bean = new Bean();
		bean.setInvoiceNumber(Database.getInstance().getLastInvoiceNumber() + 1);
		bean.setCustomerNumber(Database.getInstance().getLastCustomerNumber(Utils.CUSTOMER_COUNTER_ID) + 1);
		
		if(!Database.getInstance().insertBean(bean)) {
			JOptionPane.showMessageDialog(TablePanel.this, "Could not create Customer id: " + bean.getCustomerNumber() +". Change to something else!");
			return;
		}
		else {
			Database.getInstance().updateCustomerIdCounter(Utils.CUSTOMER_COUNTER_ID, bean.getCustomerNumber());
			if(customerIdUpdateListener != null) {
				customerIdUpdateListener.onCustomerIdUpdated();
			}
		}
		
		
		
		data.add(bean);
		tableModel.fireTableRowsInserted(data.size() - 1, data.size());
		if(refreshStatsChangeListener != null) {
			refreshStatsChangeListener.refreshStats();
		}
	}

	public void deleteRow() {
		int index = table.getSelectedRow();
		if (index != -1) {
			Bean bean = data.get(index);
			data.remove(index);
			tableModel.fireTableRowsDeleted(index - 1, index - 1);
			Database.getInstance().deleteRecord(bean);
			if(refreshStatsChangeListener != null) {
				refreshStatsChangeListener.refreshStats();
			}
		}

	}

	public void copyRow(boolean copyToApp) {
		int index = table.getSelectedRow();
		if (index != -1) {
			Bean bean = data.get(index);
			if (!copyToApp) {
				Bean newBean = new Bean(bean);
				newBean.setInvoiceNumber(Database.getInstance().getLastInvoiceNumber() + 1);
				newBean.setCustomerNumber(Database.getInstance().getLastCustomerNumber(Utils.CUSTOMER_COUNTER_ID)+1);
				if(Database.getInstance().insertBean(newBean)) {
					Database.getInstance().updateCustomerIdCounter(Utils.CUSTOMER_COUNTER_ID, newBean.getCustomerNumber());
					data.add(newBean);
					tableModel.fireTableRowsInserted(index + 1, index);
				}
				
				
			} else {
				String myString = generateCopyString(bean);
				System.out.println(myString);
				StringSelection stringSelection = new StringSelection(myString);
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents(stringSelection, null);

			}
			if(refreshStatsChangeListener != null) {
				refreshStatsChangeListener.refreshStats();
			}
		}
	}

	private String generateCopyString(Bean bean) {
		String copyString = Utils.APP_ID + "," + bean.toString();
		return copyString;
	}

	public void searchTable(String value) {
		data.clear();
		ArrayList<Bean> temp = Database.getInstance().getAllData();
		if (value.isEmpty()) {
			data.addAll(temp);
		} else {

			for (Bean bean : temp) {
				if (bean.toString().toLowerCase().contains(value)) {
					data.add(bean);
				}
			}
		}

		tableModel.fireTableDataChanged();

	}

	public Bean getSelectedBean() {
		int index = table.getSelectedRow();
		if (index != -1) {
			return this.data.get(index);
		}
		return null;
	}

	public void setUpdateInvoiceSentColumn(Bean bean) {
		if (updateBeanListener != null) {
			updateBeanListener.updateBean(bean.getInvoiceNumber(), bean);
		}
		int index = indexOf(bean);
		System.out.println(index);

		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				if (index != -1) {
					tableModel.fireTableCellUpdated(index, 23);
					tableModel.fireTableCellUpdated(index, 13);
				}
			}
		});

	}

	private int indexOf(Bean bean) {
		for (int i = 0; i < data.size(); i++) {
			if (data.get(i).getInvoiceNumber() == bean.getInvoiceNumber()) {
				data.get(i).setInvoiceSent(true);
				data.get(i).setInvoiceDate(bean.getInvoiceDate());
				return i;
			}
		}
		return -1;
	}

	public void setRefreshStatsChangeListener(RefreshStatsChangeListener refreshStatsChangeListener) {
		this.refreshStatsChangeListener = refreshStatsChangeListener;
		tableModel.setRefreshStatsChangeListener(refreshStatsChangeListener);
	}

	RefreshStatsChangeListener refreshStatsChangeListener;
	public void pasteRow() {
		Bean bean = getBeanFromClipboard();

		if (bean != null) {
			if(Database.getInstance().insertBean(bean)) {
				System.out.println("sdjflskjdf");
				Database.getInstance().updateCustomerIdCounter(Utils.CUSTOMER_COUNTER_ID, bean.getCustomerNumber());
			}
			data.add(bean);
			tableModel.fireTableRowsInserted(data.size() - 1, data.size());
			if(refreshStatsChangeListener != null) {
				refreshStatsChangeListener.refreshStats();
			}
		}
	}

	private Bean getBeanFromClipboard() {
		try {
			String data = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);

			if (data.startsWith(Utils.APP_ID)) {
				String[] beanData = data.split(",");
				Bean bean = new Bean();
				bean.setCustomerNumber(Database.getInstance().getLastCustomerNumber(Utils.CUSTOMER_COUNTER_ID) + 1);
				bean.setInvoiceNumber(Database.getInstance().getLastInvoiceNumber() + 1);
				bean.setCompanyName(beanData[2]);
				bean.setPersonName(beanData[3]);
				bean.setAddress(beanData[4]);
				bean.setZipcode(beanData[5]);
				bean.setCity(beanData[6]);
				bean.setCountry(beanData[7]);
				bean.setVatNumber(beanData[8]);
				bean.setEmailAddress(beanData[9]);
				bean.setVatPercentage(Double.parseDouble(beanData[10]));
				bean.setItemOneDescription(beanData[11]);
				bean.setAmountOfItemsOne(Integer.parseInt(beanData[12]));
				bean.setPriceItemOne(Double.parseDouble(beanData[13]));
				
				bean.setInvoiceDate(beanData[14]);
				bean.setItemTwoDescription(beanData[15]);
				System.out.println(beanData[14]);
				bean.setPriceItemTwo(Double.parseDouble(beanData[16]));
				bean.setAmountOfItemsTwo(Integer.parseInt(beanData[17]));
				bean.setItemThreeDescription(beanData[18]);
				bean.setAmountOfItemsThree(Integer.parseInt(beanData[19]));
				bean.setPriceItemThree(Double.parseDouble(beanData[20]));
				bean.setHasShipmentCost(Boolean.parseBoolean(beanData[21]));
				bean.setShipmentCostNumber(beanData[22]);
				bean.setShipmentCost(Double.parseDouble(beanData[23]));
				bean.setInvoiceSent(Boolean.parseBoolean(beanData[24]));
				bean.setAllCostCombined();

				return bean;
			}
		} catch (HeadlessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedFlavorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
