
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.naming.spi.DirStateFactory.Result;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.FileChooserUI;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import listeners.AddNewRowListener;
import listeners.CopyRowListener;
import listeners.DeleteRowListener;
import listeners.PasteRowListener;
import listeners.SearchRowListener;
import listeners.SendInvoiceListener;
import model.Bean;
import model.Database;
import model.Utils;

public class Toolbar extends JToolBar {

	private AddNewRowListener addNewRowListener;
	private DeleteRowListener deleteRowListener;
	private CopyRowListener copyRowListener;
	private SearchRowListener searchRowListener;
	private SendInvoiceListener sendInvoiceListener;
	private PasteRowListener pasteRowListener;
	private JButton addNewBtn;
	private JButton sendInvoice;
	private JButton deleteRowBtn;
	private JButton copyRowBtn;
	private JButton copyToApp;
	private JButton pasteRow;
	private JTextField searchField;
	private JProgressBar progressBar;
	private JLabel revenueMonthly;
	private JLabel revenueYearly;
	private JLabel shippingCostYearly;
	private JLabel totalTurnOver;
	private JLabel turnOverPerInvoiceThisYear;
	private JLabel turnOverByToday;
	private JLabel expectedturnOverThisYear;
	private JLabel numberOfItemsThisYear;
	private JLabel averageCostPerItem;
	private JLabel numberOfShipments;
	private JButton exportAsExcel;
	private JDatePickerImpl startDatepicker;
	private JDatePickerImpl endDatepicker;
	private UtilDateModel startModel;
	private UtilDateModel endDateModel;
	private static final String FILENAME_STRING = "Temp.xlsx";

	private boolean isStartDateSelected = false;
	private boolean isEndDateSelected = false;
	
	private JTextField lastCustNumber;
	private JButton updateCustNumber;

	private void createStartDateModel() {
		startModel = new UtilDateModel();

		// model.setDate(20,04,2014);
		// Need this...
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		JDatePanelImpl datePanel = new JDatePanelImpl(startModel, p);
		// Don't know about the formatter, but there it is...
		startDatepicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		startDatepicker.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				isStartDateSelected = true;
			}
		});

	}

	private void createEndDateModel() {
		endDateModel = new UtilDateModel();

		// model.setDate(20,04,2014);
		// Need this...
		Properties p = new Properties();
		p.put("text.today", "Today");
		p.put("text.month", "Month");
		p.put("text.year", "Year");
		JDatePanelImpl datePanel = new JDatePanelImpl(endDateModel, p);
		// Don't know about the formatter, but there it is...
		endDatepicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());

		endDatepicker.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				isEndDateSelected = true;
			}
		});

	}

	public Toolbar() {
		setLayout(new BorderLayout(10, 10));

		JPanel topPanel = new JPanel(new BorderLayout());
		JPanel buttonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));

		JPanel exportButtonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		exportButtonPanel.setBorder(BorderFactory.createLineBorder(new Color(200,200,200), 1));
		JPanel lastCustPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		lastCustPanel.setBorder(BorderFactory.createLineBorder(new Color(200,200,200), 1));
		
		
		lastCustNumber = new JTextField(8);
		lastCustNumber.setText(String.valueOf(Database.getInstance().getLastCustomerNumber(Utils.CUSTOMER_COUNTER_ID)));
		lastCustNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		
		updateCustNumber = new JButton("Update Customer Number");
		updateCustNumber.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					int customerNumber = Integer.parseInt(lastCustNumber.getText());
					Database.getInstance().updateCustomerIdCounter(Utils.CUSTOMER_COUNTER_ID,
							customerNumber);
				}
				catch (Exception ee) {
					JOptionPane.showMessageDialog(Toolbar.this, "Enter valid customer number");
				}
				
			}
		});
		lastCustPanel.add(new JLabel("Last customer number: "));
		lastCustPanel.add(lastCustNumber);
		lastCustPanel.add(updateCustNumber);
		

		addNewBtn = new JButton("New Row");
		addNewBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (addNewRowListener != null) {
					addNewRowListener.addNewBean();
				}
			}
		});

		deleteRowBtn = new JButton("Delete Row");
		deleteRowBtn.setEnabled(false);
		deleteRowBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (deleteRowListener != null) {
					deleteRowListener.deleteRow();
				}
			}
		});
		sendInvoice = new JButton("Send Invoice");
		sendInvoice.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (sendInvoiceListener != null) {
					sendInvoiceListener.sendInvoice();
				}
			}
		});

		copyRowBtn = new JButton("Copy Row");
		copyRowBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (copyRowListener != null) {
					copyRowListener.copyRow(false);
				}
			}
		});
		copyRowBtn.setEnabled(false);

		copyToApp = new JButton("Copy Row To App");
		copyToApp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (copyRowListener != null) {
					copyRowListener.copyRow(true);
				}
			}
		});
		copyToApp.setEnabled(false);

		pasteRow = new JButton("Paste Row");
		pasteRow.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (pasteRowListener != null) {
					pasteRowListener.pasteRow();
				}
			}
		});

		searchField = new JTextField();
		searchField.setPreferredSize(new Dimension(150, 28));
		searchField.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println(searchField.getText());
				if (searchRowListener != null) {
					searchRowListener.searchRow(searchField.getText());
				}
			}
		});
		searchField.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				if (searchRowListener != null) {
					searchRowListener.searchRow(searchField.getText().toString());
				}
			}

			public void removeUpdate(DocumentEvent e) {
				if (searchRowListener != null) {
					searchRowListener.searchRow(searchField.getText().toString());
				}
			}

			public void insertUpdate(DocumentEvent e) {
				if (searchRowListener != null) {
					searchRowListener.searchRow(searchField.getText().toString());
				}
			}
		});

		exportAsExcel = new JButton("Export as excel");
		exportAsExcel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				new Thread(new Runnable() {

					@Override
					public void run() {
						if (!isStartDateSelected) {
							JOptionPane.showMessageDialog(Toolbar.this, "Select start date first!");
							return;
						}
						if (!isEndDateSelected) {
							JOptionPane.showMessageDialog(Toolbar.this, "Select end date first!");
							return;
						}
						JFileChooser fileChooser = new JFileChooser();
						int result = fileChooser.showSaveDialog(Toolbar.this);
						if (result == JFileChooser.APPROVE_OPTION) {
							String fileNameString = fileChooser.getSelectedFile().getAbsolutePath();

							if (!fileNameString.endsWith(".xlsx")) {
								fileNameString += ".xlsx";
							}
							if (fileNameString != null) {
								ExcelGeneration excelGeneration = new ExcelGeneration(fileNameString);
								excelGeneration.generateFile(
										getDate(startModel.getDay(), startModel.getMonth(), startModel.getYear()),
										getDate(endDateModel.getDay(), endDateModel.getMonth(),
												endDateModel.getYear()));
								JOptionPane.showMessageDialog(Toolbar.this, "Excel file generated successfully.!");
							}
						}

					}
				}).start();
			}
		});

		JPanel statsPanel = new JPanel(new GridLayout(5, 4, 15, 5));
		statsPanel.setBorder(BorderFactory.createTitledBorder("Stats"));

		revenueMonthly = new JLabel(getMonthlyRevenue());
		statsPanel.add(new JLabel("Monthly turnover"));
		statsPanel.add(revenueMonthly);
		statsPanel.add(new JLabel("Annual turnover"));
		revenueYearly = new JLabel(getYearlyRevenue());
		statsPanel.add(revenueYearly);

		statsPanel.add(new JLabel("Total annual shipping cost"));
		shippingCostYearly = new JLabel(getYearlyShipingCost());
		statsPanel.add(shippingCostYearly);

		statsPanel.add(new JLabel("Total turnover"));
		totalTurnOver = new JLabel(getTotalTurnOver());
		statsPanel.add(totalTurnOver);

		statsPanel.add(new JLabel("Turnover per invoice this year"));
		turnOverPerInvoiceThisYear = new JLabel(getTurnOverPerInvoiceThisYear());
		statsPanel.add(turnOverPerInvoiceThisYear);

		statsPanel.add(new JLabel("Turnover per day this year"));
		turnOverByToday = new JLabel(getTurnOverByToday());
		statsPanel.add(turnOverByToday);

		statsPanel.add(new JLabel("Expected annual turnover this year"));
		expectedturnOverThisYear = new JLabel(getExpectedTurnOverThisYear());
		statsPanel.add(expectedturnOverThisYear);

		statsPanel.add(new JLabel("Number of items this year"));
		numberOfItemsThisYear = new JLabel(getNumberOfItemsThisYear());
		statsPanel.add(numberOfItemsThisYear);

		statsPanel.add(new JLabel("Average turnover per item this year"));
		averageCostPerItem = new JLabel(getAverageCostPerItemThisYear());
		statsPanel.add(averageCostPerItem);

		statsPanel.add(new JLabel("Number of shipments"));
		numberOfShipments = new JLabel(getNumberOfShipments());
		statsPanel.add(numberOfShipments);

		progressBar = new JProgressBar();
		progressBar.setVisible(false);

		buttonsPanel.add(addNewBtn);
		buttonsPanel.add(copyRowBtn);
		buttonsPanel.add(copyToApp);
		buttonsPanel.add(pasteRow);
		buttonsPanel.add(deleteRowBtn);

		createStartDateModel();
		createEndDateModel();

		exportButtonPanel.add(new JLabel("Start Date:"));
		exportButtonPanel.add(startDatepicker);
		exportButtonPanel.add(new JLabel("End Date:"));
		exportButtonPanel.add(endDatepicker);

		exportButtonPanel.add(exportAsExcel);
		// topPanel.addSeparator();
		buttonsPanel.add(searchField);
		// addSeparator();
		buttonsPanel.add(sendInvoice);
		// addSeparator();
		buttonsPanel.add(progressBar);

		JPanel southPanel = new JPanel(new GridLayout(1, 2, 10, 10));
		southPanel.add(lastCustPanel);
		southPanel.add(exportButtonPanel);
		
		topPanel.add(buttonsPanel, BorderLayout.CENTER);
		topPanel.add(southPanel, BorderLayout.SOUTH);
		// addSeparator();
		add(topPanel, BorderLayout.NORTH);
		add(statsPanel, BorderLayout.CENTER);
	}

	protected Date getDate(int day, int month, int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, day);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);
		return calendar.getTime();
	}

	private String getNumberOfShipments() {
		return String.format("%d", Database.getInstance().getNumberOfShipments());
	}

	private String getAverageCostPerItemThisYear() {
		return String.format("%.2f€",
				Database.getInstance().getCurrentYearly() / Database.getInstance().getNumberOfItemsThisYear());
	}

	private String getNumberOfItemsThisYear() {
		return String.format("%d", Database.getInstance().getNumberOfItemsThisYear());
	}

	private String getExpectedTurnOverThisYear() {
		return String.format("%.2f€", getTurnOverAsOfToday() * getTotalDaysOfYear());
	}

	private int getTotalDaysOfYear() {
		Calendar calendar = Calendar.getInstance();
		return Year.of(calendar.get(Calendar.YEAR)).length();
	}

	private String getTurnOverByToday() {
		return String.format("%.2f€", getTurnOverAsOfToday());
	}

	private double getTurnOverAsOfToday() {
		return Database.getInstance().getCurrentYearly() / getDayOfYear();
	}

	private int getDayOfYear() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.DAY_OF_YEAR);
	}

	private String getTurnOverPerInvoiceThisYear() {
		return String.format("%.2f€", Database.getInstance().getTurnOverPerInvoiceThisYear());
	}

	private String getTotalTurnOver() {
		return String.format("%.2f€", Database.getInstance().getTotalTurnOver());
	}

	private String getYearlyShipingCost() {
		return String.format("%.2f€", Database.getInstance().getShippingCostYearly());
	}

	private String getMonthlyRevenue() {
		return String.format("%.2f€", Database.getInstance().getCurrentMonthTotal());
	}

	private String getYearlyRevenue() {
		return String.format("%.2f€", Database.getInstance().getCurrentYearly());
	}

	public void refreshStats() {

		 
		revenueMonthly.setText(getMonthlyRevenue());
		revenueYearly.setText(getYearlyRevenue());
		shippingCostYearly.setText(getYearlyShipingCost());
		totalTurnOver.setText(getTotalTurnOver());
		turnOverPerInvoiceThisYear.setText(getTurnOverPerInvoiceThisYear());
		turnOverByToday.setText(getTurnOverByToday());
		averageCostPerItem.setText(getAverageCostPerItemThisYear());
		expectedturnOverThisYear.setText(getExpectedTurnOverThisYear());
		numberOfItemsThisYear.setText(getNumberOfItemsThisYear());
		numberOfShipments.setText(getNumberOfShipments());
		lastCustNumber.setText(String.valueOf(Database.getInstance().getLastCustomerNumber(Utils.CUSTOMER_COUNTER_ID)));
	}

	public void setProgressBarVisibility(boolean visibility) {
		progressBar.setVisible(visibility);
	}

	public void setProgress(int progress) {
		progressBar.setValue(progress);
	}

	public void setMaximumProgress(int maxProgress) {
		progressBar.setMaximum(maxProgress);
	}

	public void setAddNewRowListener(AddNewRowListener addNewRowListener) {
		this.addNewRowListener = addNewRowListener;
	}

	public void setPasteRowListener(PasteRowListener pasteRowListener) {
		this.pasteRowListener = pasteRowListener;
	}

	public void setSendInvoiceButtonEnabled(boolean enabled, Bean bean) {
		deleteRowBtn.setEnabled(enabled);
		copyRowBtn.setEnabled(enabled);
		copyToApp.setEnabled(enabled);
	}

	public void setDeleteRowListener(DeleteRowListener deleteRowListener) {
		this.deleteRowListener = deleteRowListener;
	}

	public void setCopyRowListener(CopyRowListener copyRowListener) {
		this.copyRowListener = copyRowListener;
	}

	public void setSearchRowListener(SearchRowListener searchRowListener) {
		this.searchRowListener = searchRowListener;
	}

	public void setSendInvoiceListener(SendInvoiceListener sendInvoiceListener) {
		this.sendInvoiceListener = sendInvoiceListener;
	}

	public void updateCustomerIdField() {
		lastCustNumber.setText(String.valueOf(Database.getInstance().getLastCustomerNumber(Utils.CUSTOMER_COUNTER_ID)));
		
	}
}
